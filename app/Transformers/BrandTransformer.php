<?php

namespace App\Transformers;

use App\Models\Brand;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * Class BrandTransformer.
 *
 * @package namespace App\Transformers;
 */
class BrandTransformer extends TransformerAbstract
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    protected $availableIncludes = [
        'country'
    ];

    /**
     * Transform the Brand entity.
     *
     * @param Brand $model
     *
     * @return array
     */
    public function transform(Brand $model)
    {
        return [
            'id' => (int)$model->id,
            'name' => $model->name,
            'slug' => $model->slug,
            'logo' => $model->logo,
            'image' => $model->image,
            'video' => $model->video,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }

    /**
     * Include country.
     *
     * @param Brand $model
     * @return Item
     */
    public function includeCountry(Brand $model)
    {
        return $this->item($model->country, new CountryTransformer);
    }
}
