<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\TempSchedule;

/**
 * Class TempScheduleTransformer.
 *
 * @package namespace App\Transformers;
 */
class TempScheduleTransformer extends TransformerAbstract
{
    /**
     * Transform the TempSchedule entity.
     *
     * @param \App\Models\TempSchedule $model
     *
     * @return array
     */
    public function transform(TempSchedule $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
