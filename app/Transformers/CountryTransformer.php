<?php

namespace App\Transformers;

use App\Models\Country;
use League\Fractal\TransformerAbstract;

/**
 * Class CountryTransformer.
 *
 * @package namespace App\Transformers;
 */
class CountryTransformer extends TransformerAbstract
{
    /**
     * Transform the Country entity.
     *
     * @param Country $model
     *
     * @return array
     */
    public function transform(Country $model)
    {
        return [
            'id' => (int)$model->id,
            'name' => $model->name,
            'slug' => $model->slug,
            'flag' => $model->flag,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
