<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Verify;

/**
 * Class VerifyTransformer.
 *
 * @package namespace App\Transformers;
 */
class VerifyTransformer extends TransformerAbstract
{
    /**
     * Transform the Verify entity.
     *
     * @param \App\Models\Verify $model
     *
     * @return array
     */
    public function transform(Verify $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
