<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\ProductRepository;
use App\Models\Product;
use App\Validators\ProductValidator;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return ProductValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     * @throws RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Get brands by given filter.
     *
     * @param Request $request
     * @return mixed
     */
    public function filter(Request $request)
    {
        $this->model = $this->model->callNamedScope('filter', [$this->model->newModelQuery(), $request]);

        return $this;
    }
}
