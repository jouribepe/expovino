<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\TempScheduleRepository;
use App\Models\TempSchedule;
use App\Validators\TempScheduleValidator;

/**
 * Class TempScheduleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TempScheduleRepositoryEloquent extends BaseRepository implements TempScheduleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TempSchedule::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return TempScheduleValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
