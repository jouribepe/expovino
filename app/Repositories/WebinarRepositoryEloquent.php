<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\WebinarRepository;
use App\Models\Webinar;
use App\Validators\WebinarValidator;

/**
 * Class WebinarRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class WebinarRepositoryEloquent extends BaseRepository implements WebinarRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Webinar::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return WebinarValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
