<?php

namespace App\Repositories\Contracts;

use Illuminate\Contracts\Foundation\Application;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BrandRepository.
 *
 * @package namespace App\Repositories\Contracts;
 * @method pushCriteria(Application $app)
 */
interface BrandRepository extends RepositoryInterface
{
    //
}
