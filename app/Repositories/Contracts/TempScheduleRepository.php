<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TempScheduleRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface TempScheduleRepository extends RepositoryInterface
{
    //
}
