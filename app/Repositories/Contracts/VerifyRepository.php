<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VerifyRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface VerifyRepository extends RepositoryInterface
{
    //
}
