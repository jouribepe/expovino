<?php

namespace App\Repositories\Contracts;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository.
 *
 * @package namespace App\Repositories\Contracts;
 *
 * @method pushCriteria(Application $app)
 */
interface ProductRepository extends RepositoryInterface
{
    /**
     * Get brands by given filter.
     *
     * @param Request $request
     * @return mixed
     */
    public function filter(Request $request);
}
