<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface WebinarRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface WebinarRepository extends RepositoryInterface
{
    //
}
