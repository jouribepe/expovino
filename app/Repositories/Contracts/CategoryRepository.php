<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository.
 *
 * @package namespace App\Repositories\Contracts;
 * @method descendantsOf(int $int)
 */
interface CategoryRepository extends RepositoryInterface
{
    //
}
