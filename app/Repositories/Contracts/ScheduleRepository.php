<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ScheduleRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface ScheduleRepository extends RepositoryInterface
{
    //
}
