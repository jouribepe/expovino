<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\VerifyRepository;
use App\Models\Verify;
use App\Validators\VerifyValidator;

/**
 * Class VerifyRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class VerifyRepositoryEloquent extends BaseRepository implements VerifyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Verify::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return VerifyValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
