<?php

namespace App\Models;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Rinvex\Categories\Models\Category as ModelCategory;

class Category extends ModelCategory implements Transformable
{
    use TransformableTrait;
}
