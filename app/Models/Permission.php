<?php

namespace App\Models;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission implements Transformable
{
    use TransformableTrait;
}
