<?php

namespace App\Models;

use App\Helpers\Categories;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Optix\Media\HasMedia;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Rinvex\Categories\Traits\Categorizable;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model implements Transformable, Buyable
{
    use TransformableTrait;
    use HasSlug;
    use SoftDeletes;
    use Categorizable;
    use Categories;
    use HasMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku',
        'shopping_sku',
        'brand_id',
        'name',
        'slug',
        'description',
        'strains',
        'origin',
        'degree',
        'breeding',
        'temperature',
        'leaks',
        'type_leaks',
        'flavor',
        'aging',
        'exclusive',
        'recommended',
        'discount',
        'discount_vip',
        'ecommerce_url',
        'image',
        'banner',
        'promotion_start_at',
        'promotion_end_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Get the brand that owns the product.
     *
     * @return BelongsTo
     */
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Get products by wine type.
     *
     * @param Builder $builder
     * @param $value
     * @return Builder
     */
    public function scopeTypeOfWine(Builder $builder, $value): Builder
    {
        return $builder->whereHas('categories', static function (Builder $query) use ($value) {
            $query->whereIn('id', self::getCategoryIdsBySlug($value));
        });
    }

    /**
     * Get products by boxes category.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function scopeBoxes(Builder $builder): Builder
    {
        return $builder->whereHas('categories', static function (Builder $query) {
            $query->whereIn('id', [17]);
        });
    }

    /**
     * Get the identifier of the Buyable item.
     *
     * @param null $options
     * @return string
     */
    public function getBuyableIdentifier($options = null) : string
    {
        return $this->shopping_sku;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @param null $options
     * @return string
     */
    public function getBuyableDescription($options = null) : string
    {
        return $this->name;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @param null $options
     * @return float
     */
    public function getBuyablePrice($options = null) : float
    {
        return $this->price;
    }
}
