<?php

namespace App\Models;

use App\Helpers\Categories;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Brand extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use HasSlug;
    use Categories;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'country_id',
        'excerpt',
        'description',
        'logo',
        'image',
        'video'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Get brand banner image if exists.
     *
     * @param $value
     * @return string
     */
    public function getImageAttribute($value) : string
    {
        return $value
            ? "brands/{$value}"
            : 'images/bg-producto_bodega.jpg';
    }

    /**
     * Get the liqueurs for the brand.
     *
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Get the country that owns the brand.
     *
     * @return BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class)->withDefault();
    }

    /**
     * Get brands by type of product.
     *
     * @param Builder $builder
     * @param $value
     *
     * @return Builder
     */
    public function scopeProductType(Builder $builder, $value): Builder
    {
        return $builder->whereHas('products.categories', static function (Builder $query) use ($value) {
            $query->whereIn('id', self::getCategoryIdsBySlug($value));
        });
    }

    /**
     * Get brands by given country.
     *
     * @param Builder $builder
     * @param $value
     * @return Builder
     */
    public function scopeByCountry(Builder $builder, $value) : Builder
    {
        return $builder->whereHas('country', static function(Builder $query) use ($value) {
            $query->where('slug', $value);
        });
    }

    /**
     * Get brands that have exclusive products.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function scopeByExclusive(Builder $builder) : Builder
    {
        return $builder->whereHas('products', static function(Builder $query) {
            $query->where('exclusive', true);
        });
    }

    /**
     * Get brands that have recommended products.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function scopeByRecommended(Builder $builder) : Builder
    {
        return $builder->whereHas('products', static function(Builder $query) {
            $query->where('recommended', true);
        });
    }

    /**
     * Get brands ordered by product price.
     *
     * @param Builder $builder
     * @param $value
     * @return Builder
     */
    public function scopeByPrice(Builder $builder, $value) : Builder
    {
        return $builder->whereHas('products', static function(Builder $query) use ($value) {
            $query->orderBy('price', $value);
        });
    }
}
