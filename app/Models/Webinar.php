<?php

namespace App\Models;

use App\Traits\Uuid;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Rinvex\Categories\Traits\Categorizable;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Webinar extends Model implements Transformable
{
    use TransformableTrait;
    use Categorizable;
    use HasSlug;
    use Uuid;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identifier',
        'title',
        'slug',
        'description',
        'embed',
        'speaker',
        'speaker_detail',
        'image',
        'start_at',
        'end_at',
        'banner',
        'capacity'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'start_at'
    ];

    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Get start at attribute.
     *
     * @param $value
     * @return Date
     * @throws Exception
     */
    public function getStartAtAttribute($value): Date
    {
        return new Date($value);
    }

    /**
     * Get end at attribute
     *
     * @param $value
     * @return Date
     * @throws Exception
     */
    public function getEndAtAttribute($value): Date
    {
        return new Date($value);
    }
}
