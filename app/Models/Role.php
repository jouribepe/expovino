<?php

namespace App\Models;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole implements Transformable
{
    use TransformableTrait;
}
