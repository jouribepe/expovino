<?php

namespace App\Providers;

use App\Repositories\BrandRepositoryEloquent;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\Contracts\BrandRepository;
use App\Repositories\Contracts\CategoryRepository;
use App\Repositories\Contracts\CountryRepository;
use App\Repositories\Contracts\ProductRepository;
use App\Repositories\Contracts\UserRepository;
use App\Repositories\CountryRepositoryEloquent;
use App\Repositories\ProductRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(CountryRepository::class, CountryRepositoryEloquent::class);
        $this->app->bind(BrandRepository::class, BrandRepositoryEloquent::class);
        $this->app->bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        $this->app->bind(ProductRepository::class, ProductRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\PermissionRepository::class, \App\Repositories\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\RoleRepository::class, \App\Repositories\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\WebinarRepository::class, \App\Repositories\WebinarRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\VerifyRepository::class, \App\Repositories\VerifyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\TempScheduleRepository::class, \App\Repositories\TempScheduleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\ScheduleRepository::class, \App\Repositories\ScheduleRepositoryEloquent::class);
        //:end-bindings:
    }
}
