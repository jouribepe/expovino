<?php

namespace App\Http\Livewire\Shoppingcart;

use Gloudemans\Shoppingcart\Facades\Cart as Shoppingcart;
use Illuminate\View\View;
use Livewire\Component;

class Checkout extends Component
{
    /**
     * @var mixed $quantity
     */
    public $quantity;

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('livewire.shoppingcart.checkout');
    }

    /**
     * Remove product to the shoppingcart.
     *
     * @param string $rowId
     */
    public function remove(string $rowId): void
    {
        Shoppingcart::remove($rowId);

        $this->emit('productAdded');
    }

    /**
     * Add product quantity.
     *
     * @param string $rowId
     */
    public function updatePlus(string $rowId): void
    {
        $this->quantity = Shoppingcart::get($rowId)->qty + 1;

        if ($this->quantity < 11) {
            Shoppingcart::update($rowId, $this->quantity);
        }

        $this->emit('productAdded');
    }

    /**
     * Remove product quantity.
     *
     * @param string $rowId
     */
    public function updateMinus(string $rowId): void
    {
        $this->quantity = Shoppingcart::get($rowId)->qty;

        if (1 !== $this->quantity) {
            --$this->quantity;
        }

        Shoppingcart::update($rowId, $this->quantity);

        $this->emit('productAdded');
    }
}
