<?php

namespace App\Http\Livewire\Shoppingcart;

use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart as Shoppingcart;
use Illuminate\View\View;
use Livewire\Component;

class Add extends Component
{
    /**
     * @var Product $product
     */
    public Product $product;

    /**
     * @param Product $product
     */
    public function mount(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return <<<'blade'
            <div class="cta-btn">
                <a href="javascript:hideNotification('pr{{ $this->product->sku }}')"
                    wire:click="addProduct">añadir al carrito</a>
            </div>
            <span id="pr{{$this->product->sku}}" class="item-agregado hide">
                <img src="{{ asset('images/check_item.png') }}" alt>
                {{ 1 }} en el carrito
            </span>
        blade;
    }

    /**
     * Add product to the shoppingcart.
     */
    public function addProduct(): void
    {
        $cart = Shoppingcart::content()->where('id', $this->product->shopping_sku)->first();

        if ($cart === null) {
            Shoppingcart::add($this->product, 1);
        } elseif ($cart->qty < 10) {
            Shoppingcart::update($cart->rowId, $cart->qty + 1);
        }

        $this->emit('productAdded');
    }
}
