<?php

namespace App\Http\Livewire\Shoppingcart;

use Cart as Shoppingcart;
use Illuminate\View\View;
use Livewire\Component;

class Cart extends Component
{
    /**
     * @var mixed $cartCount ;
     */
    public $cartCount;

    /**
     * @var string[]
     */
    protected $listeners = ['productAdded' => 'incrementCartCount'];

    /**
     * Increment cart count when product has been added.
     */
    public function incrementCartCount(): void
    {
        $this->cartCount = Shoppingcart::count();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return <<<'blade'
            <div class="carrito">
                <a href="{{ route("shoppingcart.index") }}">
                    <img src="{{ asset("images/carrito.png") }}" alt>
                    <span class="nro-items">{{ Cart::count() }}</span>
                </a>
            </div>
        blade;
    }
}
