<?php

namespace App\Http\Livewire\Gourmets\Products;

use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class Search extends Component
{
    use WithPagination;

    /**
     * @var int $perPage
     */
    public int $perPage = 6;

    /**
     * @var string[] $listeners
     */
    protected $listeners = [
        'load-more-product-gourmet' => 'loadMoreProductGourmet',
        'reset-filters-gourmet-brand' => 'resetFiltersGourmetBrand'
    ];

    /**
     * @var string[]
     */
    public array $filter = [
        'precio' => '',
        'exclusivo' => '',
    ];

    /**
     * @var string $slug
     */
    public string $slug;

    /**
     * @param string $slug
     */
    public function mount(string $slug): void
    {
        $this->slug = $slug;

        $this->filter['precio'] = '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        $products = Product::whereHas('brand', function (Builder $query) {
            $query->where('slug', $this->slug);
        });

        if ($this->filter['exclusivo']) {
            $products = $products->where('exclusive', true);
        }

        if ($this->filter['precio']) {
            $products = $products->orderBy('price', $this->filter['precio']);
        }

        if (session()->has('tipo_gourmet')) {
            $products = $products->wherehas('categories', function (Builder $builder) {
                $builder->where('slug', session()->get('tipo_gourmet'));
            });
        }

        $products = $products->whereNotNull('shopping_sku')->paginate($this->perPage);

        return view('livewire.gourmets.products.search', [
            'products' => $products,
        ]);
    }

    /**
     * Add product to shoppingcart.
     *
     * @param int $productId
     */
    public function addProduct(int $productId): void
    {
        Cart::add(Product::find($productId), 1);

        $this->emit('productAdded');
    }

    /**
     * Load mor products.
     */
    public function loadMoreProductGourmet(): void
    {
        $this->perPage += 3;
    }

    /**
     * reset filters
     */
    public function resetFiltersGourmetBrand(): void
    {
        $this->filter['precio'] = '';
        $this->filter['exclusivo'] = '';
    }
}
