<?php

namespace App\Http\Livewire\Gourmets;

use App\Helpers\Categories;
use App\Models\Brand;
use App\Models\Country;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class Search extends Component
{
    use WithPagination;

    /**
     * @var int $perPage
     */
    public int $perPage = 6;

    /**
     * @var string[] $listeners
     */
    protected $listeners = [
        'load-more-gourmet' => 'loadMoreGourmet',
        'reset-filters-gourmet' => 'resetFiltersGourmet',
    ];

    /**
     * @var string[]
     */
    public array $filter = [
        'tipo' => '',
        'pais' => '',
        'precio' => '',
        'exclusivo' => '',
    ];

    /**
     * @var string $tipo
     */
    public string $tipo = '';

    /**
     * @var Collection $countries
     */
    protected Collection $countries;

    /**
     * @var $brands
     */
    protected $brands;

    /**
     * mount component.
     */
    public function mount(): void
    {
        $this->filter['tipo'] = 'carnes';
        $this->filter['pais'] = '';
        $this->filter['precio'] = '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        $this->brands = Brand::productType($this->filter['tipo']);

        if (!empty($this->filter['pais'])) {
            $this->brands = $this->brands->byCountry($this->filter['pais']);
        }

        if ($this->filter['exclusivo']) {
            $this->brands = $this->brands->byExclusive();
        }

        if (!empty($this->filter['precio'])) {
            $this->brands = $this->brands->byPrice($this->filter['precio']);
        }

        if (!empty($this->filter['tipo'])) {
            session(['tipo_gourmet' => $this->filter['tipo']]);

            $this->countries = Country::countryByProductType($this->filter['tipo'])->get();
        }

        $this->brands = $this->brands->whereHas('products', function(Builder $query) {
            $query->whereNotNull('shopping_sku');
        });

        return view('livewire.gourmets.search', [
            'productTypes' => Categories::getCategoryByRootId(1, 2)
                ->except([
                    'tinto',
                    'blanco',
                    'rosado',
                    'espumante'
                ]),
            'countries' => $this->countries,
            'brands' =>  $this->brands->paginate($this->perPage)
        ]);
    }

    /**
     * Reset pagination where filter changed.
     */
    public function updatingFilter(): void
    {
        $this->resetPage();
    }

    /**
     * Load more content.
     */
    public function loadMoreGourmet(): void
    {
        $this->perPage += 3;
    }

    /**
     * Reset filters
     */
    public function resetFiltersGourmet() : void
    {
        $this->filter['tipo'] = 'carnes';
        $this->filter['pais'] = '';
        $this->filter['precio'] = '';
        $this->filter['exclusivo'] = '';
    }
}
