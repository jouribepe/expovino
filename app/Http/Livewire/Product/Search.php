<?php

namespace App\Http\Livewire\Product;

use App\Helpers\Categories;
use App\Models\Brand;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

/**
 * Class Search
 *
 * @property mixed product
 *
 * @package App\Http\Livewire\Product
 */
class Search extends Component
{
    use WithPagination;

    /**
     * @var int $perPage
     */
    public int $perPage = 6;

    /**
     * @var string[] $listeners
     */
    protected $listeners = [
        'load-more-product' => 'loadMoreProduct',
        'refreshBrandsList' => '$refresh'
    ];

    /**
     * @var string[]
     */
    public array $filter = [
        'tipo' => '',
        'precio' => '',
        'exclusivo' => '',
    ];

    /**
     * @var string $slug
     */
    public string $slug;

    /**
     * @var Brand $brand
     */
    public Brand $brand;

    /**
     * @var bool $showType
     */
    public bool $showType = true;

    /**
     * @var string[]
     */
    protected $queryString = [
        'page' => ['except' => 1]
    ];

    /**
     * @param string $slug
     * @param Brand $brand
     */
    public function mount(string $slug, Brand $brand): void
    {
        $this->brand = $brand;
        $this->slug = $slug;

        if (session()->has('global_wine_type') && session()->get('global_type_product') === 'vino') {
            $this->filter['tipo'] = session()->get('global_wine_type');
        } else {
            $this->filter['tipo'] = '';
        }

        $this->filter['precio'] = '';
        $this->filter['exclusivo'] = session()->has('exclusivo') ? true : '';
    }

    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        if (session()->get('global_type_product') !== 'vino') {
            $this->showType = false;
        }

        $products = Product::whereHas('brand', function (Builder $query) {
            $query->where('slug', $this->slug);
        });

        if (session()->get('global_type_product') === 'vino') {
            $categories = ['vino', 'tinto', 'blanco', 'rosado', 'espumante'];
        } else {
            $categories = [session()->get('global_type_product')];
        }

        $products = $products->whereHas('categories', function (Builder $query) use ($categories) {
            $query->whereIn('slug', $categories);
        });

        if ($this->filter['exclusivo']) {
            $products = $products->where('exclusive', true);
        }

        if ($this->filter['precio']) {
            $products = $products->orderBy('price', $this->filter['precio']);
        }

        if ($this->filter['tipo']) {
            $products = $products->typeOfWine($this->filter['tipo']);
        }

        // show only products with shopping_sku != null.
        $products = $products->whereNotNull('shopping_sku')->get();

        return view('livewire.product.search', [
            'products' => $products,
            'wineTypes' => Categories::getCategoryByRootId(2, 2)
        ]);
    }

    /**
     * Add product to shoppingcart.
     *
     * @param $productId
     */
    public function addProduct($productId): void
    {
        $product = Product::find($productId);
        $cart = Cart::content()->where('id', $product->shopping_sku)->first();

        if ($cart === null) {
            Cart::add($product, 1);
        } elseif ($cart->qty < 10) {
            Cart::update($cart->rowId, $cart->qty + 1);
        }

        $this->emit('productAdded');
    }

    /**
     * Load mor products.
     */
    public function loadMoreProduct(): void
    {
        $this->perPage += 3;
    }
}
