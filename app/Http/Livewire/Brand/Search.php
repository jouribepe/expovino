<?php

namespace App\Http\Livewire\Brand;

use App\Helpers\Categories;
use App\Models\Brand;
use App\Models\Country;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class Search extends Component
{
    use WithPagination;

    /**
     * @var int $perPage
     */
    public int $perPage = 6;

    /**
     * @var bool $showVino
     */
    public bool $showVino = true;

    /**
     * @var string[] $listeners
     */
    protected $listeners = [
        'load-more' => 'loadMore',
        'reset-filters' => 'resetFilters',
    ];

    /**
     * @var string[]
     */
    public array $filter = [
        'tipo' => '',
        'vino' => '',
        'pais' => '',
        'exclusivo' => '',
    ];

    /**
     * @var string $tipo
     */
    public string $tipo = '';

    /**
     * @var $brands
     */
    protected $brands;

    /**
     * @var Collection $countries
     */
    protected Collection $countries;

    /**
     * @var string $search
     */
    public string $search = '';

    /**
     * @var string[]
     */
    protected $queryString = [
        'search' => ['except' => ''],
        'tipo' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    /**
     * mount component.
     */
    public function mount(): void
    {
        $this->search = '';

        if (session()->has('global_type_product')) {
            $this->filter['tipo'] = session()->get('global_type_product');
        } else {
            $this->filter['tipo'] = 'vino';
        }

        if (session()->has('exclusivo')) {
            session()->forget('exclusivo');
        }

        $this->filter['pais'] = '';
        $this->filter['vino'] = '';
    }

    /**
     * @return Application|Factory|View
     */
    public function render()
    {

        if (session()->has('global_wine_type')) {
            session()->forget('global_wine_type');
        }

        $this->brands = Brand::productType($this->filter['tipo']);

        if (!empty($this->search)) {
            $this->brands = $this->brands->where('name', 'like', '%' . $this->search . '%');
        }

        if (!empty($this->filter['pais'])) {
            $this->brands = $this->brands->byCountry($this->filter['pais']);
        }

        if(!empty($this->filter['vino'])) {
            session(['global_wine_type' => $this->filter['vino']]);

            $this->brands = $this->brands->whereHas('products.categories', function (Builder $query) {
               $query->whereIn('slug', [$this->filter['vino']]);
            });
        }

        if ($this->filter['exclusivo'] ) {
            session(['exclusivo' => true]);

            $this->brands = $this->brands->byExclusive();
        }

        if (!empty($this->filter['tipo'])) {
            if (session()->has('global_type_product') && session()->get('global_type_product') !== $this->filter['tipo']) {
                $this->emit('reset-filters');
            }

            session(['global_type_product' => $this->filter['tipo']]);

            $this->countries = Country::countryByProductType($this->filter['tipo'])->get();
        }

        if (session()->has('global_type_product') && session()->get('global_type_product') !== 'vino') {
            $this->showVino = false;
        } else {
            $this->showVino = true;
        }

        // show only products with shopping_sku != null.
        $this->brands = $this->brands->whereHas('products', function (Builder $query) {
            $query->whereNotNull('shopping_sku');
        });

        // Order brands by name asc
        $this->brands = $this->brands->orderBy('name', 'asc');

        return view('livewire.brand.search', [
            'brands' => $this->brands->paginate($this->perPage),
            'productTypes' => Categories::getCategoryByRootId(1)
                ->except(['boxes', 'gourmet', 'copas']),
            'countries' => $this->countries
        ]);
    }

    /**
     * @return string
     */
    public function paginationView(): string
    {
        return 'vendor.pagination.expovinos';
    }

    /**
     * Reset pagination.
     */
    public function updatingSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Reset pagination where filter changed.
     */
    public function updatingFilter(): void
    {
        $this->resetPage();
    }

    /**
     * Load more content.
     */
    public function loadMore(): void
    {
        $this->perPage += 3;
    }

    /**
     * Reset all filters.
     */
    public function resetFilters(): void
    {
        $this->search = '';
        $this->filter['pais'] = '';
        $this->filter['vino'] = '';
        $this->filter['exclusivo'] = '';
    }
}
