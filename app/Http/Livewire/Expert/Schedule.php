<?php

namespace App\Http\Livewire\Expert;

use App\Models\User;
use App\Models\Verify;
use App\Models\Webinar;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;
use Jenssegers\Date\Date;
use Livewire\Component;

class Schedule extends Component
{
    /**
     * @var string $day
     */
    public string $day;

    /**
     * @var $webinar_id
     */
    public $webinar_id;

    /**
     * @var User $user
     */
    public User $user;

    /**
     * @var int $box
     */
    public int $box;

    /**
     * @var int $scheduled
     */
    public int $scheduled = 0;

    public function mount(): void
    {
        $this->day = Date::create(2020, 9, 24);
        $this->user = User::where('email', auth()->user()->email)->first();
        $this->scheduled = \App\Models\Schedule::where('user_id', $this->user->id)->count();

        $this->box = Verify::where('document_nro', $this->user->document_nro)
            ->orWhere('document_nro', $this->user->email)
            ->first()->box;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        $dates = Webinar::whereHas('categories', function (Builder $query) {
            $query->where('slug', 'expertovino');
        });

        if ($this->box === 5) {
            $dates = $dates->whereIn('box', [1, 2, 3, 4]);
        } else {
            $dates = $dates->where('box', $this->box);
        }

        $dates = $dates
            ->get()
            ->groupBy(function ($webinar) {
                return $webinar->start_at->format('Y-m-d');
            });

        $hours = Webinar::whereHas('categories', function (Builder $query) {
            $query->where('slug', 'expertovino');
        });

        if ($this->box === 5) {
            $hours = $hours->whereIn('box', [1, 2, 3, 4]);
        } else {
            $hours = $hours->where('box', $this->box);
        }

        $hours = $hours
            ->whereBetween('start_at', [
                Date::parse($this->day)->hours(0)->minutes(0)->seconds(0),
                Date::parse($this->day)->addHours(23)->addMinutes(59)->addSeconds(59)
            ])
            ->get()
            ->groupBy(function ($webinar) {
                return "{$webinar->start_at->format('H:i')} - {$webinar->end_at->format('H:i')}";
            });

        $wIds = \App\Models\Schedule::where('user_id', $this->user->id)->pluck('webinar_id');

        return view('livewire.expert.schedule', [
            'dates' => $dates,
            'hours' => $hours,
            'schedules' => Webinar::whereIn('id', $wIds)->get()
        ]);
    }

    /**
     * Save schedule user.
     */
    public function save(): void
    {
        $this->scheduled = \App\Models\Schedule::where('user_id', $this->user->id)->count();

        if ($this->scheduled < $this->box) {
            \App\Models\Schedule::create([
                'user_id' => $this->user->id,
                'webinar_id' => $this->webinar_id
            ]);
        }
    }
}
