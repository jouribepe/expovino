<?php

namespace App\Http\Livewire\Boxes;

use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\View\View;
use Livewire\Component;

class Search extends Component
{
    /**
     * @var int $perPage
     */
    public int $perPage = 6;

    /**
     * @var string $precio
     */
    public string $precio= '';

    /**
     * @var string[] $listeners
     */
    protected $listeners = [
        'load-more-boxes' => 'loadMoreBoxes'
    ];

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('livewire.boxes.search', [
            'products' => Product::boxes()
                ->orderBy('price', !empty($this->precio) ? $this->precio : 'asc')
                ->get()
        ]);
    }

    /**
     * Load more boxes.
     */
    public function loadMoreBoxes(): void
    {
        $this->perPage += 3;
    }

    /**
     * Add product to shoppingcart.
     *
     * @param int $productId
     */
    public function addProduct(int $productId): void
    {
        Cart::add(Product::find($productId), 1);

        $this->emit('productAdded');
    }
}
