<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandCreateRequest;
use App\Http\Requests\BrandUpdateRequest;
use App\Repositories\Contracts\BrandRepository;
use App\Validators\BrandValidator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class BrandsController.
 *
 * @package namespace App\Http\Controllers;
 */
class BrandsController extends Controller
{
    /**
     * @var BrandRepository
     */
    protected $repository;

    /**
     * @var BrandValidator
     */
    protected $validator;

    /**
     * BrandsController constructor.
     *
     * @param BrandRepository $repository
     * @param BrandValidator $validator
     */
    public function __construct(BrandRepository $repository, BrandValidator $validator)
    {
        $this->middleware(['auth', 'history']);

        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|JsonResponse|Response|View
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $brands = $this->repository->with(['products', 'country'])->paginate(6);

        if (request()->wantsJson()) {
            return response()->json($brands);
        }

        return view('brands.index', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandCreateRequest $request
     *
     * @return JsonResponse|RedirectResponse|Response
     */
    public function store(BrandCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $brand = $this->repository->create($request->all());

            $response = [
                'message' => 'Brand created.',
                'data' => $brand->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     *
     * @return Application|Factory|JsonResponse|Response|View
     */
    public function show(string $slug)
    {
        $brand = $this->repository->findByField('slug', $slug)->first();
        $products = $brand->products()->paginate(6);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $brand,
            ]);
        }

        return view('pages.brands.show', compact('brand', 'products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Application|Factory|Response|View
     */
    public function edit(int $id)
    {
        $brand = $this->repository->find($id);

        return view('brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandUpdateRequest $request
     * @param int $id
     *
     * @return JsonResponse|RedirectResponse|Response
     */
    public function update(BrandUpdateRequest $request, int $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $brand = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Brand updated.',
                'data' => $brand->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return JsonResponse|RedirectResponse|Response
     */
    public function destroy(int $id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Brand deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Brand deleted.');
    }
}
