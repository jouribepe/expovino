<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Product;
use App\Repositories\Contracts\ProductRepository;
use App\Validators\ProductValidator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;
use JsonException;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ProductsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProductsController extends Controller
{
    /**
     * @var ProductRepository $repository
     */
    protected ProductRepository $repository;

    /**
     * @var ProductValidator $validator
     */
    protected ProductValidator $validator;

    /**
     * ProductsController constructor.
     *
     * @param ProductRepository $repository
     * @param ProductValidator $validator
     */
    public function __construct(ProductRepository $repository, ProductValidator $validator)
    {
        $this->middleware(['auth', 'history']);

        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|JsonResponse|Response|View
     */
    public function index()
    {
        $this->repository->pushCriteria(app(RequestCriteria::class));
        $products = $this->repository->filter(request())->with('brand')->paginate(6);

        if (request()->wantsJson()) {
            return response()->json($products);
        }

        return view('products.index', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductCreateRequest $request
     *
     * @return JsonResponse|RedirectResponse|Response
     */
    public function store(ProductCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $product = $this->repository->create($request->all());

            $response = [
                'message' => 'Product created.',
                'data' => $product->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     *
     * @return Application|Factory|JsonResponse|Response|View
     */
    public function show(string $slug)
    {
        $product = $this->repository->findByField('slug', $slug)->first();

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $product,
            ]);
        }

        return view('pages.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Application|Factory|Response|View
     */
    public function edit(int $id)
    {
        $product = $this->repository->find($id);

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param int $id
     *
     * @return JsonResponse|RedirectResponse|Response
     */
    public function update(ProductUpdateRequest $request, int $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $product = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Product updated.',
                'data' => $product->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return JsonResponse|RedirectResponse|Response
     */
    public function destroy(int $id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Product deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Product deleted.');
    }

    /**
     * Update product from the commerce API.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws JsonException
     */
    public function price(int $id): RedirectResponse
    {
        $api = "http://wongfood.vtexcommercestable.com.br/api/catalog_system/pub/products/search?fq=productClusterIds:{$id}";

        $products = json_decode(Http::get($api)->body(), false, 512, JSON_THROW_ON_ERROR);

        foreach ($products as $product) {
            if (isset($product)) {
//                $productSku = key(get_object_vars(json_decode(collect($product->SkuData)
//                    ->first(), false, 512, JSON_THROW_ON_ERROR)));

                $productSku = $product->productReference;

                $dbProduct = Product::whereSku($productSku)->first();

                if ($dbProduct !== null)
                {
                    $dbProduct->price = collect(collect($product->items)->first()->sellers)
                        ->first()
                        ->commertialOffer
                        ->Price;

                    $dbProduct->save();
                }
            }
        }

        return redirect()->to('home');
    }
}
