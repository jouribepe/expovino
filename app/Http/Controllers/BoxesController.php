<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class BoxesController extends Controller
{
    /**
     * BoxesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'history']);
    }

    /**
     * Show boxes index page.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('pages.boxes.index');
    }

    /**
     * Show box page.
     *
     * @param string $slug
     * @return Application|Factory|View
     */
    public function show(string $slug)
    {
        return view('pages.boxes.show')->with('product', Product::whereSlug($slug)->first());
    }
}
