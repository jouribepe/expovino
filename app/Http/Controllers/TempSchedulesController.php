<?php

namespace App\Http\Controllers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\TempScheduleCreateRequest;
use App\Http\Requests\TempScheduleUpdateRequest;
use App\Repositories\Contracts\TempScheduleRepository;
use App\Validators\TempScheduleValidator;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class TempSchedulesController.
 *
 * @package namespace App\Http\Controllers;
 */
class TempSchedulesController extends Controller
{
    /**
     * @var TempScheduleRepository
     */
    protected $repository;

    /**
     * @var TempScheduleValidator
     */
    protected $validator;

    /**
     * TempSchedulesController constructor.
     *
     * @param TempScheduleRepository $repository
     * @param TempScheduleValidator $validator
     */
    public function __construct(TempScheduleRepository $repository, TempScheduleValidator $validator)
    {
        $this->middleware(['auth', 'role:catador|super-admin', 'history']);

        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse|Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app(RequestCriteria::class));
        $tempSchedules = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $tempSchedules,
            ]);
        }

        return view('tempSchedules.index', compact('tempSchedules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TempScheduleCreateRequest $request
     *
     * @return JsonResponse|\Illuminate\Http\RedirectResponse|Response|null
     *
     */
    public function store(TempScheduleCreateRequest $request)
    {
        $this->repository->create($request->all());

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tempSchedule = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $tempSchedule,
            ]);
        }

        return view('tempSchedules.show', compact('tempSchedule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tempSchedule = $this->repository->find($id);

        return view('tempSchedules.edit', compact('tempSchedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TempScheduleUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TempScheduleUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $tempSchedule = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'TempSchedule updated.',
                'data'    => $tempSchedule->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'TempSchedule deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'TempSchedule deleted.');
    }
}
