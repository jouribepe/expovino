<?php

namespace App\Http\Controllers;

use App\Imports\BrandsImport;
use App\Imports\ProductsImport;

class ImportsController extends Controller
{
    /**
     * ImportsController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:super-admin', 'history']);
    }

    /**
     * Imports brands to database.
     */
    public function brands()
    {
        $import = new BrandsImport();
        $import->sheets();

        \Excel::import($import, storage_path("Expovinos2020-02.xlsx"));
    }

    /**
     * Import products to database.
     */
    public function products()
    {
        $import = new ProductsImport();
        $import->sheets();

        \Excel::import($import, storage_path("Expovinos2020-02.xlsx"));
    }
}
