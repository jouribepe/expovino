<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class GourmetsController extends Controller
{
    /**
     * GourmetsController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'history']);
    }

    /**
     * Show gourmets page index.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('pages.gourmets.index');
    }

    /**
     * Show gourmet brand page.
     *
     * @param string $slug
     * @return Application|Factory|View
     */
    public function show(string $slug)
    {
        return view('pages.gourmets.show')->with('brand', Brand::whereSlug($slug)->first());
    }

    /**
     * Show product gourmet page.
     *
     * @param string $slug
     * @return Application|Factory|View
     */
    public function product(string $slug)
    {
        return view('pages.gourmets.product')->with('product', Product::whereSlug($slug)->first());
    }
}
