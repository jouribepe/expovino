<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;

/**
 * Class ShoppingcartController
 *
 * @package App\Http\Controllers
 */
class ShoppingcartController extends Controller
{
    /**
     * ShoppingcartController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'history']);
    }

    /**
     * Show Shoppingcart view.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('pages.shoppingcart.index');
    }

    /**
     * Add item to the shoppingcart
     *
     * @param Product $product
     * @return RedirectResponse
     */
    public function add(Product $product): RedirectResponse
    {
        Cart::add($product, 1);

        return redirect()->back();
    }

    /**
     * Remove item to the shoppingcart.
     *
     * @param string $rowId
     * @return RedirectResponse
     */
    public function remove(string $rowId): RedirectResponse
    {
        Cart::remove($rowId);

        return redirect()->back();
    }

    /**
     * Update product quantity in the cart content.
     *
     * @param string $rowId
     * @param string $type
     * @return JsonResponse|RedirectResponse
     */
    public function quantity(string $rowId, string $type)
    {
        $qty = Cart::get($rowId)->qty;

        if ($type === 'plus') {
            $qty++;
        } elseif ($qty > 1) {
            $qty--;
        }

        Cart::update($rowId, $qty);

        return redirect()->back();
    }

    /**
     * Redirect to commerce shoppingcart.
     *
     * @return RedirectResponse
     */
    public function store(): RedirectResponse
    {
        $variables = [
            "accountName" => 'wongfood',
            'domain' => 'www.wong.pe',
            'seller' => 'wong'
        ];

        $orderItems = [];

        foreach (Cart::content() as $item) {
            $seller = $variables['seller'];

            //if (Product::where('shopping_sku', $item->id)->first()->hasCategories(17)) {
            //  $seller = "wong";
            //}

            $orderItems[] = [
                'id' => $item->id,
                'seller' => $seller,
                'quantity' => $item->qty
            ];
        }

        // Get the order form id.
        $orderFormId = Http::get("https://{$variables['accountName']}.vtexcommercestable.com.br/api/checkout/pub/orderForm")['orderFormId'];

        // Add items to the order.
        Http::post("https://{$variables['accountName']}.vtexcommercestable.com.br/api/checkout/pub/orderForm/{$orderFormId}/items",
            [
                "orderItems" => $orderItems
            ]);

        // Redirection Cart Page : "https://{$variables['domain']}/checkout/?orderFormId={$orderFormId}#/cart"
        // Redirection Checkout Page: "https://{$variables['domain']}/checkout/?orderFormId={$orderFormId}#/payment"

        return redirect()->to("https://{$variables['domain']}/checkout/?orderFormId={$orderFormId}#/shipping");
    }
}
