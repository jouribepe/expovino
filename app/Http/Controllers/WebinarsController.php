<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\WebinarCreateRequest;
use App\Http\Requests\WebinarUpdateRequest;
use App\Repositories\Contracts\WebinarRepository;
use App\Validators\WebinarValidator;

/**
 * Class WebinarsController.
 *
 * @package namespace App\Http\Controllers;
 */
class WebinarsController extends Controller
{
    /**
     * @var WebinarRepository
     */
    protected $repository;

    /**
     * @var WebinarValidator
     */
    protected $validator;

    /**
     * WebinarsController constructor.
     *
     * @param WebinarRepository $repository
     * @param WebinarValidator $validator
     */
    public function __construct(WebinarRepository $repository, WebinarValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $webinars = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $webinars,
            ]);
        }

        return view('webinars.index', compact('webinars'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  WebinarCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(WebinarCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $webinar = $this->repository->create($request->all());

            $response = [
                'message' => 'Webinar created.',
                'data'    => $webinar->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $webinar = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $webinar,
            ]);
        }

        return view('webinars.show', compact('webinar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $webinar = $this->repository->find($id);

        return view('webinars.edit', compact('webinar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  WebinarUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(WebinarUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $webinar = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Webinar updated.',
                'data'    => $webinar->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Webinar deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Webinar deleted.');
    }
}
