<?php

namespace App\Http\Controllers;

use App\Models\TempSchedule;
use App\Models\Webinar;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;

/**
 * Class WinesController
 *
 * @package App\Http\Controllers
 */
class WinesController extends Controller
{

    /**
     * WinesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'history']);
    }

    /**
     * Show cata vino view.
     *
     * @return Application|Factory|View
     */
    public function cata()
    {
        $catas = Webinar::whereHas('categories', function (Builder $builder) {
            $builder->where('id', 31);
        })->get();

        return view("pages.wines.cata")->with('catas', $catas);
    }

    /**
     * Show cata vino details view.
     *
     * @param string $slug
     * @return Application|Factory|View
     */
    public function cataDetails(string $slug)
    {
        $webinar = Webinar::where('slug', $slug)->where('box', null)->first();

        return view("pages.wines.cata-details")->with('webinar', $webinar);
    }

    /**
     * Show hablemos vino page.
     *
     * @return Application|Factory|View
     */
    public function hablemos()
    {
        $hablemos = Webinar::whereHas('categories', function (Builder $builder) {
            $builder->where('id', 30);
        })->get();

        return view("pages.wines.hablemos")->with('hablemos', $hablemos);
    }

    /**
     * Show hablemos vino details page.
     *
     * @param string $slug
     * @return Application|Factory|View
     */
    public function hablemosDetails(string $slug)
    {
        $webinar = Webinar::where('slug', $slug)->where('box', null)->first();

        return view("pages.wines.hablemos-details")->with('webinar', $webinar);
    }

    /**
     * Show viva vino page.
     *
     * @return Application|Factory|View
     */
    public function viva()
    {
        return view("pages.wines.viva");
    }

    /**
     * Show experto vino page.
     *
     * @return Application|Factory|View
     */
    public function experto()
    {

        $scheduled = TempSchedule::where('user_id', auth()->user()->id)->first();

        return view("pages.wines.experto")->with([
            'scheduled' => $scheduled
        ]);
    }
}
