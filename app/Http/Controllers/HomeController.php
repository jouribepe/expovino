<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Webinar;
use App\Repositories\Contracts\BrandRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;
use Jenssegers\Date\Date;

class HomeController extends Controller
{
    /**
     * @var BrandRepository
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @param BrandRepository $repository
     */
    public function __construct(BrandRepository $repository)
    {
        $this->middleware(['auth', 'history']);

        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $products = Product::where('promotion_start_at', '<=', now())
            ->where('promotion_end_at', '>=', now())
            ->take(3)
            ->get();

        $catas = Webinar::whereHas('categories', function (Builder $query) {
            $query->where('id', 31);
        })
            ->where('start_at', '<=', Date::now())
            ->where('end_at', '>=', Date::now())
            ->get();

        $hablemos = Webinar::whereHas('categories', function (Builder $query) {
            $query->where('id', 30);
        })
            ->where('start_at', '<=', Date::now())
            ->where('end_at', '>=', Date::now())
            ->get();

        return view('home')->with([
            'products' => $products,
            'catas' => $catas,
            'hablemos' => $hablemos
        ]);
    }

    /**
     * return crono view.
     *
     * @return Application|Factory|View
     */
    public function crono()
    {
        return view('pages.crono');
    }
}
