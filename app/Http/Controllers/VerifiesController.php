<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\VerifyCreateRequest;
use App\Http\Requests\VerifyUpdateRequest;
use App\Repositories\Contracts\VerifyRepository;
use App\Validators\VerifyValidator;

/**
 * Class VerifiesController.
 *
 * @package namespace App\Http\Controllers;
 */
class VerifiesController extends Controller
{
    /**
     * @var VerifyRepository
     */
    protected $repository;

    /**
     * @var VerifyValidator
     */
    protected $validator;

    /**
     * VerifiesController constructor.
     *
     * @param VerifyRepository $repository
     * @param VerifyValidator $validator
     */
    public function __construct(VerifyRepository $repository, VerifyValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $verifies = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $verifies,
            ]);
        }

        return view('verifies.index', compact('verifies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VerifyCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(VerifyCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $verify = $this->repository->create($request->all());

            $response = [
                'message' => 'Verify created.',
                'data'    => $verify->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $verify = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $verify,
            ]);
        }

        return view('verifies.show', compact('verify'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $verify = $this->repository->find($id);

        return view('verifies.edit', compact('verify'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VerifyUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(VerifyUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $verify = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Verify updated.',
                'data'    => $verify->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Verify deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Verify deleted.');
    }
}
