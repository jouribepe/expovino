<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class XSSGuard
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $response->headers->set('X-XSS-Protection', '1; mode=block', false);

        return $response;
    }
}
