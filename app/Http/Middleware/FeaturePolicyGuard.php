<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class FeaturePolicyGuard
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $response->headers->set('Feature-Policy', "vibrate 'none'; geolocation 'none'" , false);

        return $response;
    }
}
