<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

/**
 * Class BrandsImport
 *
 * @package App\Imports
 */
class BrandsImport implements WithMultipleSheets
{
    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            'Brands' => new FirstSheetImport(),
        ];
    }
}
