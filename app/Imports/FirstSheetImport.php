<?php

namespace App\Imports;

use App\Models\Brand;
use App\Models\Country;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FirstSheetImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            $country = Country::whereName($row['country'])->first();

            if ($country !== null) {
                Brand::create([
                    'name' => Str::title($row['name']),
                    'country_id' => Country::whereName($row['country'])->first()->id,
                    'excerpt' => Str::ucfirst(Str::lower(Str::limit($row['description'], 120, '...'))),
                    'description' => Str::ucfirst($row['description']),
                    'logo' => $row['logo'],
                    'image' => $row['image'],
                    'video' => $row['video']
                ]);
            }
        }
    }
}
