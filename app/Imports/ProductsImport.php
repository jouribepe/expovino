<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ProductsImport implements WithMultipleSheets
{

    /**
     * @inheritDoc
     */
    public function sheets(): array
    {
        return [
            'Wines' => new SecondSheetImport(),
        ];
    }
}
