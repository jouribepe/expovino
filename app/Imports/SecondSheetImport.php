<?php

namespace App\Imports;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SecondSheetImport implements ToCollection, WithHeadingRow
{
    /**
     * @inheritDoc
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            if ($row['sku'] !== 'por codificar') {
                $brand = Brand::whereName(Str::title($row['brand']))->first();

                if ($brand !== null) {
                    $product = Product::create([
                        'sku' => $row['sku'],
                        'brand_id' => $brand->id,
                        'name' => Str::title($row['name']),
                        'strains' => $row['strains'],
                        'origin' => $row['origin'],
                        'degree' => $row['degree'],
                        'breeding' => $row['breeding'],
                        'temperature' => $row['temperature'],
                        'exclusive' => Str::lower($row['exclusive']) === 'no' ? false : true,
                        'recommended' => Str::lower($row['recommended']) === 'no' ? false : true,
                        'image' => "/products/{$row['image']}"
                    ]);

                    $wineType = Category::where('id', 2)->first();
                    $wineTypeId = $wineType
                        ->descendants()
                        ->get()
                        ->where('name', $row['wine_type'])
                        ->first();

                    if ($wineTypeId !== null) {
                        $product->attachCategories($wineTypeId->id);
                    }

                    $sugarContent = Category::where('id', 2)->first();
                    $sugarContentId = $sugarContent
                        ->descendants()
                        ->get()
                        ->where('name', $row['sugar_content'])
                        ->first();

                    if ($sugarContentId !== null) {
                        $product->attachCategories($sugarContentId->id);
                    }
                }
            }
        }
    }
}
