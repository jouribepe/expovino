<?php

namespace App\Presenters;

use App\Transformers\CountryTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CountryPresenter.
 *
 * @package namespace App\Presenters;
 */
class CountryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new CountryTransformer();
    }
}
