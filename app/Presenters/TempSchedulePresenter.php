<?php

namespace App\Presenters;

use App\Transformers\TempScheduleTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TempSchedulePresenter.
 *
 * @package namespace App\Presenters;
 */
class TempSchedulePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TempScheduleTransformer();
    }
}
