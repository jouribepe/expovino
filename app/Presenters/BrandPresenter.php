<?php

namespace App\Presenters;

use App\Transformers\BrandTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BrandPresenter.
 *
 * @package namespace App\Presenters;
 */
class BrandPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new BrandTransformer();
    }
}
