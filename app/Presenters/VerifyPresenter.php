<?php

namespace App\Presenters;

use App\Transformers\VerifyTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class VerifyPresenter.
 *
 * @package namespace App\Presenters;
 */
class VerifyPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new VerifyTransformer();
    }
}
