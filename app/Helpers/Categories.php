<?php

namespace App\Helpers;

use App\Models\Category;
use Illuminate\Support\Collection;

trait Categories
{
    /**
     * Get categories by given root id.
     *
     * @param int $id
     * @param int $depth
     * @return mixed
     */
    public static function getCategoryByRootId(int $id, int $depth = 1)
    {
        return Category::whereId($id)
            ->firstOrFail()
            ->descendants()
            ->withDepth()
            ->having('depth', '=', $depth)
            ->pluck('name', 'slug');
    }

    /**
     * Get categories by given slug.
     *
     * @param $value
     * @return Collection
     */
    public static function getCategoryIdsBySlug($value) : Collection
    {
        $category = Category::whereSlug($value)->firstOrFail();

        return $category->descendantsAndSelf($category->id)->pluck('id');
    }
}
