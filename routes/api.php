<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('login', 'API\AuthController@login');
//Route::post('register', 'API\AuthController@register');
//
//Route::middleware('auth:api')->prefix('v1')->group(function () {
//
//    Route::get('/user', function (Request $request) {
//        return $request->user();
//    });
//
//    Route::resource('bodegas', 'BrandsController');
//    Route::resource('productos', 'ProductsController');
//});
