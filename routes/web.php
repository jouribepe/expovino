<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$blocked = static function() {
    Route::get('/', static function () {
        return view("block");
    });
};

Route::group(['domain' => 'www.expovino.com.pe'], $blocked);
Route::group(['domain' => 'expovino.com.pe'], $blocked);

Route::get('/', function () {
    if (!\auth()->guest()) {
        return \route('home');
    }
    return view('auth.login');
})->middleware('guest');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cronograma', 'HomeController@crono')->name('crono');

Route::resource('bodegas', 'BrandsController');
Route::resource('productos', 'ProductsController');
Route::resource('agenda', 'TempSchedulesController');

//Wines
Route::get('cata-vino', 'WinesController@cata')
    ->name('wines.cata-vino');
Route::get('cata-vino/detalle/{slug}', 'WinesController@cataDetails')->name('wines.cata-vino.details');
Route::get('experto-vino', 'WinesController@experto')
    ->middleware('role:catador|super-admin')
    ->name('wines.experto-vino');
Route::get('hablemos-vino', 'WinesController@hablemos')->name('wines.hablemos-vino');
Route::get('hablemos-vino/detalle/{slug}', 'WinesController@hablemosDetails')
    ->name('wines.hablemos-vino.details');
Route::get('viva-vino', 'WinesController@viva')->name('wines.viva-vino');

//Shoppingcart
Route::get('carrito', 'ShoppingcartController@index')->name('shoppingcart.index');
Route::get('shoppingcart/add/{product}', 'ShoppingcartController@add')->name('shoppingcart.add');
Route::get('shoppingcart/remove/{product}', 'ShoppingcartController@remove')->name('shoppingcart.remove');
Route::get('shoppingcart/update/{rowId}/{qty}',
    'ShoppingcartController@quantity')->name('shoppingcart.quantity.update');
Route::get('shoppingcart/store', 'ShoppingcartController@store')->name('shoppingcart.store');

// Products
Route::get('productos/update/price/{id}', 'ProductsController@price')->middleware(['role:super-admin']);

//Imports
Route::get('imports/brands', 'ImportsController@brands')->name('import.brands');
Route::get('imports/products', 'ImportsController@products')->name('import.products');

//Boxes
Route::get('boxes', 'BoxesController@index')->name('boxes.index');
Route::get('boxes/{slug}', 'BoxesController@show')->name('boxes.show');

// Gourmets
Route::get('gourmet', 'GourmetsController@index')->name('gourmets.index');
Route::get('gourmet/{slug}', 'GourmetsController@show')->name('gourmets.show');
route::get('gourmet/producto/{productSlug}', "GourmetsController@product")->name('gourmets.product.show');

// Copas
Route::middleware(['auth', 'history'])->get('copas/copa-red-wine-56cl', function () {
    return view('pages.products.show')->with(
        'product', \App\Models\Product::with('brand')
        ->with('categories')
        ->whereSlug('copa-red-wine-56cl')
        ->first()
    );
})->name('copas');

//Route::get('file', function() {
//    return view('file');
//});
//
//Route::post('upload', function() {
//    $file = request()->file('file');
//
//    \Optix\Media\MediaUploader::fromFile($file)->upload();
//});

Auth::routes();
