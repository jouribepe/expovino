<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateProductsTable.
 */
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('sku')->unique();

            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');

            $table->string('name')->index();
            $table->string('slug');
            $table->float('price')->nullable();
            $table->string('strains')->nullable();
            $table->string('origin')->nullable();
            $table->string('degree')->nullable();
            $table->string('breeding')->nullable();
            $table->string('temperature')->nullable();
            $table->string('leaks')->nullable();
            $table->string('type_leaks')->nullable();
            $table->string('flavor')->nullable();
            $table->string('aging')->nullable();
            $table->boolean('exclusive')->nullable();
            $table->boolean('recommended')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('discount_vip')->nullable();
            $table->string('ecommerce_url')->nullable();
            $table->string('image')->nullable();
            $table->string('banner')->nullable();
            $table->dateTime('promotion_start_at')->nullable();
            $table->dateTime('promotion_end_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
