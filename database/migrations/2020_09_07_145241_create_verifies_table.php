<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateVerifiesTable.
 */
class CreateVerifiesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void
    {
		Schema::create('verifies', static function(Blueprint $table) {
            $table->id();
            $table->enum('document_type', ['DNI', 'RUC', 'CE'])->default('DNI');
            $table->string('document_nro', 11)->unique();
            $table->enum('role', ['vip', 'catador'])->default('vip');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void
    {
		Schema::drop('verifies');
	}
}
