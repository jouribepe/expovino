<?php

/** @var Factory $factory */

use App\Models\Brand;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'excerpt' => $faker->paragraph(2),
        'description' => $faker->paragraph(4, true),
        'country_id' => $faker->numberBetween(1, 9),
        'logo' => $faker->imageUrl(283, 169, 'nature', true),
        'image' => $faker->imageUrl(1920, 1080, 'nature', true),
        'video' => 'https://youtu.be/CJ6UwEmMz5I'
    ];
});
