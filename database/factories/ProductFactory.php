<?php

/** @var Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'sku' => $faker->unique()->isbn13,
        'brand_id' => $faker->numberBetween(1, 30),
        'name' => $faker->company,
        'degree' => $faker->randomFloat(2, 10, 80),
        'price' => $faker->randomNumber(2),
    ];
});

$factory->afterCreating(Product::class, function (Product $product, Faker $faker) {
    $categoryId = $faker->numberBetween(2, 5);

    $product->attachCategories($categoryId);

    if ($categoryId === 2) {
        $wineType = $faker->numberBetween(7, 10);

        $product->attachCategories($wineType);

        if ($wineType === 7) {
            $product->attachCategories($faker->numberBetween(12, 14));
        }

        $product->strains = $faker->numberBetween(10, 100) . '% ' . $faker->randomElement(['Malbec', 'Cabernet']);
        $product->origin = $faker->country;
        $product->breeding = $faker->numberBetween(6, 60) . ' meses';
        $product->exclusive = $faker->boolean(30);
        $product->recommended = $faker->boolean(60);
        $product->discount = $faker->numberBetween(0, 70);
        $product->image = $faker->imageUrl(600, 800, 'nature');
        $product->banner = $faker->imageUrl(1920, 600, 'nature');
        $product->promotion_start_at = $faker->dateTimeBetween('now', 'now', 'America/Lima');
        $product->promotion_end_at = $faker->dateTimeBetween('now', '+ 7 days', 'America/Lima');
    } else {
        $product->ecommerce_url = $faker->url;
        $product->discount_vip = $faker->numberBetween(10, 70);
        $product->image = $faker->imageUrl(600, 800, 'nature');
    }

    $product->save();
});
