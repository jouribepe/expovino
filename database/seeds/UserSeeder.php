<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        User::create([
            'first_name' => 'Jorge',
            'last_name' => 'Ortega Uribe',
            'email' => 'jouribepe@gmail.com',
            'password' => bcrypt('41484547'),
            'document_type' => 'DNI',
            'document_nro' => '41484547',
            'is_accept_policies' => true
        ]);

        User::create([
            'first_name' => 'Juan',
            'last_name' => 'Perez Romero',
            'email' => 'juan@romero.com',
            'password' => bcrypt('44444444'),
            'document_type' => 'DNI',
            'document_nro' => '44444444',
            'is_accept_policies' => true
        ]);

        User::create([
            'first_name' => 'Julian',
            'last_name' => 'Morales Arias',
            'email' => 'julian@morales.com',
            'password' => bcrypt('88888888'),
            'document_type' => 'DNI',
            'document_nro' => '88888888',
            'is_accept_policies' => true
        ]);

        User::create([
            'first_name' => 'Lourdes',
            'last_name' => 'Cardenas',
            'email' => 'lourdes@cardenas.com',
            'password' => bcrypt('11114444'),
            'document_type' => 'DNI',
            'document_nro' => '11114444',
            'is_accept_policies' => true
        ]);

        User::create([
            'first_name' => 'Laura',
            'last_name' => 'Landauro León',
            'email' => 'llaura20@hotmail.com',
            'password' => bcrypt('88887777'),
            'document_type' => 'DNI',
            'document_nro' => '88887777',
            'is_accept_policies' => true
        ]);

        User::create([
            'first_name' => 'Pedro',
            'last_name' => 'Rosales',
            'email' => 'pedro@rosales.com',
            'password' => bcrypt('74747474'),
            'document_type' => 'DNI',
            'document_nro' => '74747474',
            'is_accept_policies' => true
        ]);

        User::create([
            'first_name' => 'Usuario',
            'last_name' => 'General',
            'email' => 'usuario1@correo.com',
            'password' => bcrypt('12312312'),
            'document_type' => 'DNI',
            'document_nro' => '12312312',
            'is_accept_policies' => true
        ]);

        User::create([
            'first_name' => 'Usuario',
            'last_name' => 'General',
            'email' => 'usuario2@correo.com',
            'password' => bcrypt('23423434'),
            'document_type' => 'DNI',
            'document_nro' => '23423434',
            'is_accept_policies' => true
        ]);
    }
}
