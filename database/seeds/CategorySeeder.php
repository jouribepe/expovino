<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        \App\Models\Category::create([
            'name' => [
                'es' => 'Tipo Producto'
            ],

            'children' => [
                [
                    'name' => [
                        'es' => 'Vino'
                    ],

                    'children' => [
                        [
                            'name' => ['es' => 'Tinto'],
                        ],
                        [
                            'name' => ['es' => 'Blanco'],
                        ],
                        [
                            'name' => ['es' => 'Rosado'],
                        ],
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Whisky'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Vodka'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Gin'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Pisco'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Ron'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Tequila'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Complemento'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Agua Tónica'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Digest Pousse'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Aperitivo'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Brandy'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Boxes'
                    ],
                ],
                [
                    'name' => [
                        'es' => 'Gourmet'
                    ],
                    'children' => [
                        [
                            'name' => ['es' => 'Carnes'],
                        ],
                        [
                            'name' => ['es' => 'Congelados'],
                        ],
                        [
                            'name' => ['es' => 'Delicatessen Gourmet'],
                        ],
                        [
                            'name' => ['es' => 'Fiambres'],
                        ],
                        [
                            'name' => ['es' => 'Quesos'],
                        ],
                    ],

                ],
                [
                    'name' => [
                        'es' => 'Copas'
                    ],
                ],
            ],
        ]);

        \App\Models\Category::create([
            'name' => [
                'es' => 'Contenido Azúcar'
            ],
            'children' => [
                [
                    'name' => ['es' => 'Seco'],
                ],
                [
                    'name' => ['es' => 'Semi seco'],
                ],
                [
                    'name' => ['es' => 'Dulce'],
                ],
            ],
        ]);

        \App\Models\Category::create([
            'name' => [
                'es' => 'Tipo Webinar'
            ],

            'children' => [
                [
                    'name' => ['es' => 'HablemosVino'],
                ],
                [
                    'name' => ['es' => 'CataVino'],
                ],
                [
                    'name' => ['es' => 'ExpertoVino'],
                ],
            ],
        ]);
    }
}
