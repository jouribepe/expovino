<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $countries = [
            ['name' => 'Argentina', 'flag' => 'flag_argentina.png'],
            ['name' => 'Chile', 'flag' => 'flag_chile.png'],
            ['name' => 'España', 'flag' => 'flag_espana.png'],
            ['name' => 'Estados Unidos', 'flag' => 'flag_estados_unidos.png'],
            ['name' => 'Francia', 'flag' => 'flag_francia.png'],
            ['name' => 'Italia', 'flag' => 'flag_italia.png'],
            ['name' => 'Líbano', 'flag' => 'flag_libano.png'],
            ['name' => 'Sudáfrica', 'flag' => 'flag_sudafrica.png'],
            ['name' => 'Perú', 'flag' => 'flag_peru.png'],
            ['name' => 'Bolivia', 'flag' => 'flag_bolivia.png'],
            ['name' => 'Cuba', 'flag' => 'flag_cuba.png'],
            ['name' => 'Escocia', 'flag' => 'flag_escocia.png'],
            ['name' => 'Georgia', 'flag' => 'flag_georgia.png'],
            ['name' => 'Guatemala', 'flag' => 'flag_guatemala.png'],
            ['name' => 'Inglaterra', 'flag' => 'flag_inglaterra.png'],
            ['name' => 'Irlanda', 'flag' => 'flag_irlanda.png'],
            ['name' => 'Jamaica', 'flag' => 'flag_jamaica.png'],
            ['name' => 'México', 'flag' => 'flag_mexico.png'],
            ['name' => 'Nicaragua', 'flag' => 'flag_nicaragua.png'],
            ['name' => 'Panamá', 'flag' => 'flag_panama.png'],
            ['name' => 'Polonia', 'flag' => 'flag_polonia.png'],
            ['name' => 'Puerto Rico', 'flag' => 'flag_puerto_rico.png'],
            ['name' => 'República Dominicana', 'flag' => 'flag_republica_dominicana.png'],
            ['name' => 'Suecia', 'flag' => 'flag_suecia.png'],
            ['name' => 'Venezuela', 'flag' => 'flag_venezuela.png'],
        ];

        foreach ($countries as $country) {
            Country::create($country);
        }
    }
}
