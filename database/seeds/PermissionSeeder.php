<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        // create permissions
        Permission::create(['name' => 'ver home']);
        Permission::create(['name' => 'ver bodegas']);
        Permission::create(['name' => 'ver vinos']);
        Permission::create(['name' => 'ver viva vino']);
        Permission::create(['name' => 'ver hablemos vino']);
        Permission::create(['name' => 'ver experto vino']);
        Permission::create(['name' => 'ver cata vino']);

        // create roles and assign existing permissions
        $general = Role::create(['name' => 'general']);
        $general->givePermissionTo('ver home');
        $general->givePermissionTo('ver bodegas');
        $general->givePermissionTo('ver vinos');
        $general->givePermissionTo('ver viva vino');
        $general->givePermissionTo('ver hablemos vino');

        $vip = Role::create(['name' => 'vip']);
        $vip->givePermissionTo('ver home');
        $vip->givePermissionTo('ver bodegas');
        $vip->givePermissionTo('ver vinos');
        $vip->givePermissionTo('ver viva vino');
        $vip->givePermissionTo('ver hablemos vino');
        $vip->givePermissionTo('ver cata vino');

        $catador = Role::create(['name' => 'catador']);
        $catador->givePermissionTo('ver home');
        $catador->givePermissionTo('ver bodegas');
        $catador->givePermissionTo('ver vinos');
        $catador->givePermissionTo('ver viva vino');
        $catador->givePermissionTo('ver hablemos vino');
        $catador->givePermissionTo('ver experto vino');
        $catador->givePermissionTo('ver cata vino');

        $super = Role::create(['name' => 'super-admin']);

        $user1 = User::find(1);
        $user2 = User::find(2);
        $user3 = User::find(3);
        $user4 = User::find(4);
        $user5 = User::find(5);
        $user6 = User::find(6);

        $user1->assignRole($super);
        $user2->assignRole($general);
        $user3->assignRole($vip);
        $user4->assignRole($vip);
        $user5->assignRole($catador);
        $user6->assignRole($catador);
    }
}
