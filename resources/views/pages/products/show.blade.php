@extends('layouts.app')

@section('content')
    <main id="main">
        <section class="foto-bodega">
            <div class="img">
                <picture>
                    <source srcset="{{ asset("images/bg-producto_bodega_m.jpg") }}"
                            media="(min-width: 300px) and (max-width: 768px)">
                    <img src="{{ asset("images/bg-producto_bodega.jpg") }}" alt="Wong | Bodegas Lagarde">
                </picture>
            </div>
        </section>

        <div class="contenido">
            <section class="breadcrumb">
                <div class="content">
                    <p>
                        <a href="{{ url()->previous() }}"><img src="{{ asset("images/back.png") }}" alt> Regresar</a>
                    </p>
                </div>
            </section>
            <section class="bodega-vino">
                <div class="content">
                    <div class="detalle">
                        <img src="{{ asset("brands/" . str_replace("../brands/", "", $product->brand->logo) ) }}" alt="{{ $product->brand->name }}">
                        <div class="name">
                            <h2>{{ $product->brand->name }}</h2>
                            <p class="country">
                                <img src="{{ asset("images/" . $product->brand->country->flag) }}"
                                     alt="{{ $product->brand->country->name }}">
                                {{ $product->origin }}, <strong>{{ $product->brand->country->name }}</strong>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="detalle-producto">
                <div class="content">
                    <div class="detalle">
                        <div class="imagen">
                            <img src="{{ asset("products/{$product->image}")  }}" alt="{{ $product->name }}">
                        </div>
                        <div class="ficha-producto">
                            <h2>{{ $product->name }}</h2>
                            <ul>
                                @if(in_array($product->categories()->first()->id, [2, 3, 4, 5, 26, 27, 28, 33], true))
                                    <li>
                                        <p class="cepa">Cepa: <span>{{ $product->strains }}</span></p>
                                    </li>
                                @endif
                                @if($product->slug !== 'copa-red-wine-56cl')
                                    <li>
                                        <p class="tipo">Tipo:
                                            <span>
                                            {{ $product->categories()->first()->name }}
                                        </span>
                                        </p>
                                    </li>

                                    <li>
                                        <p class="grado">Grado alcohólico: <span>{{ $product->degree }}</span></p>
                                    </li>
                                    <li>
                                        <p class="crianza">Crianza: <span>{{ $product->breeding }}</span></p>
                                    </li>
                                @endif
                                @if(in_array($product->categories()->first()->id, [2, 3, 4, 5, 26, 27, 28, 33], true))
                                    <li>
                                        <p class="temperatura">Temperatura recomendada:
                                            <span>{{ $product->temperature }}</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="azucar">Cant. Azúcar:

                                            <span>{{ $product->categories()->whereIn('id', [26, 27, 28])->first()->name ?? '' }}</span>
                                        </p>
                                    </li>
                                @endif
                                @if($product->categories()->first()->id === 7)
                                    <li>
                                        <p># Filtraciones: <span>{{ $product->leaks ?? '' }}</span></p>
                                    </li>
                                    <li>
                                        <p>Tipo Filtraciones: <span>{{ $product->type_leaks ?? '' }}</span></p>
                                    </li>
                                @endif
                                @if($product->categories()->first()->id === 8)
                                    <li>
                                        <p>Perfil del sabor: <span>{{ $product->flavor ?? '' }}</span></p>
                                    </li>
                                @endif
                                @if($product->categories()->first()->id === 6)
                                    <li>
                                        <p>Añejamiento: <span>{{ $product->aging ?? '' }}</span></p>
                                    </li>
                                @endif
                                @if($product->slug !== 'copa-red-wine-56cl')
                                    <li>
                                        <p class="bodega">Bodega: <span>{{ $product->brand->name }}</span></p>
                                    </li>
                                @endif
                                @if($product->slug === 'copa-red-wine-56cl')
                                    <li>
                                        {{ $product->description }}
                                    </li>
                                @endif

                                <li>
                                    <p class="produccion">
                                        <img src="{{ asset("images/" . $product->brand->country->flag) }}"
                                             alt="{{  $product->brand->country->name }}"> Región:
                                        <span>{{ $product->origin }}</span>
                                    </p>
                                </li>
                            </ul>

                            <div class="price">
                                <p>Precio:
                                    <span>S/
                                        <span class="monto">{{ number_format($product->price, 2) }}</span>
                                    </span>
                                </p>
                                @if($product->price !== $product->price_regular)
                                    <p class="regular-price">Precio regular: <span>S/{{ number_format($product->price_regular, 2) }}</span></p>
                                @endif
                            </div>

                            <!-- @livewire('shoppingcart.add', ['product' => $product]) -->

                        </div>
                    </div>
                </div>
            </section>
            <section class="disclaimer">
                <div class="content">
                    <p>Tomar bebidas alcohólicas en exceso es dañino</p>
                    <p>Prohibida la venta a menores de 18 años</p>
                </div>
            </section>
        </div>
    </main>

    <script>
        function hideNotification(sku) {
            document.getElementById(sku).classList.remove('hide')

            window.setTimeout(function () {
                document.getElementById(sku).classList.add('hide')
            }, 450)
        }
    </script>
@endsection
