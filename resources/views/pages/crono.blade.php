@extends("layouts.app")

@section("content")
    <main id="main" class="cronograma">

        <section class="breadcrumb">
            <div class="content">
                <p>
                    <a href="{{ url()->previous() }}"><img src="{{ asset("images/back.png") }}" alt> Regresar</a>
                </p>
            </div>
        </section>

        <section class="cronograma-content">
            <div class="content">
                <div class="title">
                    <img src="{{ asset("images/crono_page.png") }}" alt>
                    <h2>Cronograma</h2>
                </div>
                <div class="bajada">
                    <h3>CRONOGRAMA DE EVENTOS</h3>
                    <p>Conoce el cronograma completo de todos los eventos del ExpoVino</p>
                </div>
                <div class="cronograma-crono">
                    <div class="content">
                        <div class="crono-table">
                            <div class="table-tabs">
                                <ul class="tabs">
                                    <li class="tab">
                                        <a href="#diaUno">Jueves 24</a>
                                    </li>
                                    <li class="tab">
                                        <a class="active" href="#diaDos">Viernes 25</a>
                                    </li>
                                    <li class="tab">
                                        <a href="#diaTres">Sábado 26</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="diaUno" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <!--<tr>
                                        <td>
                                            <div class="horarios">
                                                <span>02:30 p.m. - 03:00 p.m.</span>
                                                <span>03:00 p.m. - 03:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <div id="sale-exclusive" class="label sale-exclusive">
                                                        <h2>Contenido</h2>
                                                        <span>Exclusivo</span>
                                                    </div>
                                                    <h3>ExpertoVino</h3>
                                                    <p>Cata de vinos rosado, tinto y tintos roble.</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>¿Cómo reconocer un buen vino con el olor?</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>08:00 p.m. - 08:30 p.m.</span>
                                                <span>08:30 p.m. - 09:00 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Misiones de Rengo</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>Chivas Regal</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>09:00 p.m. - 09:30 p.m.</span>
                                                <span>09:30 p.m. - 10:00 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Tarapacá</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>El Enemigo</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="diaDos" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>07:00 p.m. - 07:30 p.m.</span>
                                                <span>07:30 p.m. - 08:00 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Intipalka Malbec</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>08:00 p.m. - 08:30 p.m.</span>
                                                <span>08:30 p.m. - 09:00 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Lapostolle</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>Johnnie Walker</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>09:00 p.m. - 09:30 p.m.</span>
                                                <span>09:30 p.m. - 10:00 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Faustino</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Concha y Toro</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="diaTres" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>02:30 p.m. - 03:00 p.m.</span>
                                                <span>03:00 p.m. - 03:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Intipalka Sauvignon Blanc</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>Lagarde</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>03:30 p.m. - 04:00 p.m.</span>
                                                <span>04:00 p.m. - 04:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Nieto Senetiner</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>04:30 p.m. - 05:00 p.m.</span>
                                                <span>05:00 p.m. - 05:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Cadus</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>Cartavio</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>05:30 p.m. - 06:00 p.m.</span>
                                                <span>06:00 p.m. - 06:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Lapostolle Cavernet Sauvignon</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>06:30 p.m. - 07:00 p.m.</span>
                                                <span>07:00 p.m. - 07:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>Doña Paula</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>Romovi Catena</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td>
                                            <div class="horarios">
                                                <span>07:30 p.m. - 08:00 p.m.</span>
                                                <span>08:00 p.m. - 08:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>El secreto de El Enemigo</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>08:30 p.m. - 09:00 p.m.</span>
                                                <span>09:00 p.m. - 09:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>Vittoria</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>HablemosVino</h3>
                                                    <p>Viña Cobos</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td>
                                            <div class="horarios">
                                                <span>09:30 p.m. - 10:00 p.m.</span>
                                                <span>10:00 p.m. - 10:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>El secreto de El Enemigo</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="horarios">
                                                <span>10:30 p.m. - 11:00 p.m.</span>
                                                <span>11:00 p.m. - 11:30 p.m.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>CataVino</h3>
                                                    <p>El secreto de El Enemigo</p>
                                                </div>
                                                @hasanyrole("vip|catador|super-admin")
                                                <div class="cta-btn">
                                                    <a href="javascript:void(0);" onclick="showModal('modal-cronograma')" class="soon"><span>Agendar</span></a>
                                                </div>
                                                @endhasanyrole
                                            </div>
                                        </td>
                                    </tr>-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
