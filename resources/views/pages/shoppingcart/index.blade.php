@extends('layouts.app')

@section('content')
    <main id="main" class="carrito">
        <section class="breadcrumb">
            <div class="content">
                <p>
                    <a href="{{ url()->previous() }}">
                        <img src="{{ asset("images/back.png") }}" alt> Regresar
                    </a>
                </p>
            </div>
        </section>

        <section class="carrito-content">
            <div class="content">
                <div class="title">
                    <img src="{{ asset("images/carrito-page.png") }}" alt>
                    <h2>Carrito de Compras</h2>
                </div>

                @livewire('shoppingcart.checkout')

            </div>
        </section>
    </main>
@endsection
