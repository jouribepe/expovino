@extends('layouts.app')

@section('content')
    <main id="main">
        <section class="breadcrumb">
            <div class="content">
                <p>
                    <a href="{{ url("/home") }}">
                        <img src="{{ asset("images/back.png") }}" alt> Regresar
                    </a>
                </p>
            </div>
        </section>
        <section class="bodega-home gourmet">
            @livewire('gourmets.search')
        </section>
    </main>
@endsection
