@extends('layouts.app')

@section('content')
    <main id="main">
        <section class="foto-bodega">
            <div class="img">
                <picture>
                    <source srcset="{{ asset('images/bg-producto_bodega_m.jpg') }}"
                            media="(min-width: 300px) and (max-width: 768px)">
                    <img src="{{ asset('images/bg-producto_bodega.jpg') }}" alt="Wong | Bodegas Lagarde">
                </picture>
            </div>
        </section>
        <div class="contenido">
            <section class="breadcrumb">
                <div class="content">
                    <p>
                        <a href="{{ url()->previous() }}">
                            <img src="{{ asset('images/back.png') }}" alt=""> Regresar
                        </a>
                    </p>
                </div>
            </section>
            <section class="bodega-vino">
                <div class="content">
                    <div class="detalle">
                        <img src="{{ asset("brands/" . str_replace("../brands/", "", $product->brand->logo) )  }}" alt="{{ $product->brand->name }}">
                        <div class="name">
                            <h2>{{ $product->brand->name }}</h2>
                            <p class="country">
                                <img src="{{ asset("images/{$product->brand->country->flag}") }}"
                                     alt="{{ $product->brand->country->name }}">
                                {{ $product->origin }} ,<strong>{{ $product->brand->country->name }}</strong>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="detalle-producto gastronomia">
                <div class="content">
                    <div class="detalle">
                        <div class="imagen">
                            <img src="{{ asset("products/{$product->image}") }}"
                                 alt="{{ $product->name }}">
                        </div>
                        <div class="ficha-producto">
                            <h2>{{ $product->name }}</h2>
                            {!! $product->description !!}
                            <div class="price">
                                <p>Precio:
                                    <span>S/
                                        <span class="monto">
                                            {{ number_format($product->price, 2) }}
                                        </span>
                                    </span>
                                </p>
                                @if($product->price !== $product->price_regular)
                                    <p class="regular-price">Precio regular: <span>S/ {{ number_format($product->price_regular, 2) }}</span></p>
                                @endif
                            </div>

                            @livewire('shoppingcart.add', ['product' => $product])

                        </div>
                    </div>
                </div>
            </section>
            <section class="disclaimer">
                <div class="content">
                    <p>Tomar bebidas alcohólicas en exceso es dañino</p>
                    <p>Prohibida la venta a menores de 18 años</p>
                </div>
            </section>
        </div>
    </main>

    <script>
        function hideNotification(sku) {
            document.getElementById(sku).classList.remove('hide')

            window.setTimeout(function () {
                document.getElementById(sku).classList.add('hide')
            }, 450)
        }
    </script>
@endsection
