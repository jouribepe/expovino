@extends('layouts.app')

@section('content')
    <main id="main">
        <section class="foto-bodega">
            <div class="img">
                <picture>
                    <source srcset="{{ asset('images/bg-producto_bodega_m.jpg') }}"
                            media="(min-width: 300px) and (max-width: 768px)">
                    <img src="{{ asset('images/bg-producto_bodega.jpg') }}" alt="Wong | Bodegas Lagarde">
                </picture>
            </div>
        </section>

        <div class="contenido gastronomia">

            <section class="breadcrumb">
                <div class="content">
                    <p>
                        <a href="{{ url('/gourmet') }}">
                            <img src="{{ asset('images/back.png') }}" alt=""> Regresar
                        </a>
                    </p>
                </div>
            </section>

            <section class="la-bodega">
                <div class="content @if(empty($brand->video)) no-video @endif">
                    <div class="detalle">
                        <img src="{{  asset("brands/{$brand->logo}")  }}" alt="{{ $brand->name }}">
                        <div class="name">
                            <h2>{{ $brand->name }}</h2>
                            <p class="country">
                                <img src="{{ asset("images/{$brand->country->flag}") }}" alt="">
                                <strong>{{ $brand->country->name }}</strong>
                            </p>
                        </div>
                        <p>{{ $brand->description }}</p>
                    </div>
                    @if(!empty($brand->video))
                        <div class="video-iframe">
                            <iframe width="560" height="315" src="{{ $brand->video }}" frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                    @endif
                </div>
            </section>

            @livewire('gourmets.products.search', ['slug' => $brand->slug])

            <section class="disclaimer">
                <div class="content">
                    <p>Tomar bebidas alcohólicas en exceso es dañino</p>
                    <p>Prohibida la venta a menores de 18 años</p>
                </div>
            </section>
        </div>
    </main>
@endsection
