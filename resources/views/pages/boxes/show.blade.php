@extends('layouts.app')

@section('content')
    <main id="main">
        <section class="foto-bodega">
            <div class="img">
                <picture>
                    <source srcset="{{ asset('images/m_fondo_box.png') }}"
                            media="(min-width: 300px) and (max-width: 768px)">
                    <img src="{{ asset('images/fondo_box.jpg') }}" alt="">
                </picture>
            </div>
        </section>
        <div class="contenido boxes">
            <section class="breadcrumb">
                <div class="content">
                    <p>
                        <a href="{{ url()->previous() }}">
                            <img src="{{ asset('images/back.png') }}" alt=""> Regresar
                        </a>
                    </p>
                </div>
            </section>

            <section class="bodega-vino">
                <div class="content">
                    <div class="detalle">
                        <img src="{{ asset('images/box-espovino.jpg') }}" alt>
                        <div class="name">
                            <h2>Boxes <br>Cata Vino</h2>
                        </div>
                    </div>
                </div>
            </section>

            <section class="detalle-producto boxes">
                <div class="content">
                    <div class="detalle">
                        <div class="imagen">
                            <div id="galeria-box" class="galeria-box single-item">
                                @foreach($product->getMedia('box') as $item)
                                    <div class="item">
                                        <img src="{{ asset("products/{$item->file_name}") }}" alt="">
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="ficha-producto">
                            <h2>{{ $product->name }}</h2>
                            {!! $product->description !!}
                            <div class="price">
                                <p>Precio:
                                    <span>S/
                                        <span class="monto">{{ number_format($product->price, 2) }}</span>
                                    </span>
                                </p>
                            </div>

                            <!-- @livewire('shoppingcart.add', ['product' => $product]) -->

                        </div>
                    </div>
                </div>
            </section>
            <section class="disclaimer">
                <div class="content">
                    <p>Tomar bebidas alcohólicas en exceso es dañino</p>
                    <p>Prohibida la venta a menores de 18 años</p>
                </div>
            </section>
        </div>
    </main>

    <script>
        function hideNotification(sku) {
            document.getElementById(sku).classList.remove('hide')

            window.setTimeout(function () {
                document.getElementById(sku).classList.add('hide')
            }, 450)
        }
    </script>
@endsection
