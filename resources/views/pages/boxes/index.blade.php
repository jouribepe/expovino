@extends('layouts.app')

@section('content')
    <main id="main">
        <section class="foto-bodega">
            <div class="img">
                <picture>
                    <source srcset="{{ asset('images/m_fondo_box.png') }}"
                            media="(min-width: 300px) and (max-width: 768px)">
                    <img src="{{ asset('images/fondo_box.jpg') }}" alt="Wong | Bodegas Lagarde">
                </picture>
            </div>
        </section>
        <div class="contenido boxes">
            <section class="breadcrumb">
                <div class="content">
                    <p>
                        <a href="{{ url('/home') }}"><img src="{{ asset('images/back.png') }}" alt=""> Regresar</a>
                    </p>
                </div>
            </section>
            <section class="la-bodega boxes">
                <div class="content">
                    <div class="detalle">
                        <div class="name">
                            <h2>Boxes Cata Vino</h2>
                        </div>
                        <!-- <p>Al comprar cualquier de estos boxes accedes a catas virtuales, donde <br>podrás descubrir
                            todos los secretos de los vinos del box, de la mano <br>de los embajadores de las marcas.
                        </p>
                        <div class="cta-btn">
                            <a href="{{ route('crono') }}">Ver cronograma de Catas</a>
                        </div> -->
                    </div>
                </div>
            </section>

            @livewire('boxes.search')

            <section class="disclaimer">
                <div class="content">
                    <p>Tomar bebidas alcohólicas en exceso es dañino</p>
                    <p>Prohibida la venta a menores de 18 años</p>
                </div>
            </section>

        </div>
    </main>
@endsection
