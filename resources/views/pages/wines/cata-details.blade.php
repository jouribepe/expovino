@extends("layouts.app")

@section("content")
    <main id="main" class="cata">
        <section class="breadcrumb">
            <div class="content">
                <p>
                    <a href="{{ route('wines.cata-vino') }}"><img src="{{ asset("images/back.png") }}" alt=""> Regresar</a>
                </p>
            </div>
        </section>

        @if(isset($webinar))
            <section class="cataDt-content">
                <div class="content">
                    <div class="title">
                        <img src="{{ asset("images/cata-page.png") }}" alt="">
                        <h2>CataVino</h2>
                    </div>
                    <div class="bajada">
                        <h3>{{ $webinar->title }}</h3>
                        <p>{{ $webinar->description }}</p>
                    </div>
                    <div class="cataDt-video">
                        <div class="content">
                            <div class="video" style="width: 100%">
                                <iframe width="560" height="315" src=" {{ 'https://www.youtube.com/embed/' . $webinar->embed }}" frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
{{--                            <div class="chat">--}}
{{--                                <iframe width="560" height="315" src="https://www.youtube.com/live_chat?v={{ $webinar->embed  }}&embed_domain=expovino.com.pe" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </section>
        @endif
    </main>
@endsection
