@extends("layouts.app")

@section("content")
    <main id="main" class="cata">
    <!-- <section class="breadcrumb">
            <div class="content">
                <p>
                    <a href="{{ url()->previous() }}">
                        <img src="{{ asset("images/back.png") }}" alt> Regresar
                    </a>
                </p>
            </div>
        </section> -->

        <section class="cata-content">
            <div class="content">
                <div class="title">
                    <img src="{{ asset("images/cata-page.png") }}" alt="Cata Vino">
                    <h2>CataVino</h2>
                </div>
                <div class="bajada">
                    <h3>CRONOGRAMA DE EVENTOS</h3>
                    <p>Conoce el cronograma de eventos de CataVino que tenemos preparado para ti</p>
                </div>
                <div class="cata-cronograma">
                    <div class="content">
                        <div class="crono-table">
                            <div class="table-tabs">
                                <ul class="tabs">
                                    <li class="tab">
                                        <a href="#diaUno">Jueves 24</a>
                                    </li>
                                    <li class="tab">
                                        <a class="active" href="#diaDos">Viernes 25</a>
                                    </li>
                                    <li class="tab">
                                        <a href="#diaTres">Sábado 26</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="diaUno" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/misiones-de-rengo.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">08:00 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 23 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Misiones de Rengo</h3>
                                                    <p>Viviana Magnere nos invita a conocer una de las marcas de vino más reconocida en Chile.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Viviana Magnere</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box3|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 23 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 23 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Misiones+de+Rengo&from=2020-09-25+01%3A00%3A00&to=2020-09-25+02%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Misiones+de+Rengo')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/tarapaca.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">09:00 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 24 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Tarapacá</h3>
                                                    <p>La Viña Tarapacá nos invita a descubrir más de 140 años de tradición produciendo los vinos más prestigiosos de Chile.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Sebastián Ruiz Flaño</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box3|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 24 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 24 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Tarapacá&from=2020-09-25+02%3A00%3A00&to=2020-09-25+03%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Tarapacá')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/el-enemigo.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">09:00 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 26 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>El Enemigo</h3>
                                                    <p>Alejandro Vigil nos invita a descubrir cómo canaliza su energía creativa tratando de desafiar los límites de la viticultura y enología
                                                        tradicionales.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Alejandro Vigil</h4>
                                                    <p></p>
                                                </div>
                                            </div>

                                            @hasanyrole("box5|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 26 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 26 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=El+Enemigo&from=2020-09-25+02%3A00%3A00&to=2020-09-25+03%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=El+Enemigo')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="diaDos" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/vinas-queirolo.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">07:00 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 25 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Intipalka Malbec</h3>
                                                    <p>De generosas tierras peruanas nacen los vinos Intipalka y La Bodega Queirolo nos invita a conocerlos.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Luis Gomez</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box1|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 25 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                               @elseif($cata->id === 25 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Intipalka&from=2020-09-26+00%3A00%3A00&to=2020-09-26+01%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Intipalka')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/lapostolle.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">08:00 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 27 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Lapostolle</h3>
                                                    <p>La Bodega Lapostolle de la mano de Charles De Bournet nos invita a conocer sus vinos más emblemáticos, de gran estructura, con taninos suaves,
                                                        aterciopelados y un largo final.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Charles de Bournet</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box6|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 27 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 27 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Lapostolle&from=2020-09-26+01%3A00%3A00&to=2020-09-26+02%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Lapostolle')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/faustino.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">09:00 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 28 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Faustino</h3>
                                                    <p>Junto a Carlos Moreno, La Bodega Faustino nos invita a conocer uno de los vinos clásicos de la Rioja.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Carlos Moreno</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box2|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 28 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 28 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Faustino&from=2020-09-26+02%3A00%3A00&to=2020-09-26+03%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Faustino')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/concha-y-toro.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">09:00 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 29 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Concha y Toro</h3>
                                                    <p>La Viña Concha y Toro nos presenta una marca de vino creada bajo los conceptos de tradición, innovación y diversidad.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Rodrigo Alonso</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box5|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 29 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                               @elseif($cata->id === 29 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Viña+Concha+y+Toro&from=2020-09-26+02%3A00%3A00&to=2020-09-26+03%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Viña+Concha+y+Toro')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="diaTres" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/vinas-queirolo.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">02:30 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 30 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Intipalka Sauvignon Blanc</h3>
                                                    <p>De generosas tierras peruanas nacen los vinos Intipalka y La Bodega Queirolo nos invita a conocerlos.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Luis Gomez</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box1|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 30 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 30 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Viñas+Queirolo&from=2020-09-26+19%3A30%3A00&to=2020-09-26+20%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Viñas+Queirolo')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/nieto-senetiner.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">03:30 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 31 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Nieto Senetiner</h3>
                                                    <p>De los Valles de Uco y Lujan de Cuyo, Santiago Mayorga nos invita a descubrir más de 130 años de tradición vitivinícola.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Santiago Mayorga</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box4|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 31 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 31 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Nieto+Senetiner&from=2020-09-26+20%3A30%3A00&to=2020-09-26+21%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Nieto+Senetiner')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/nieto-senetiner.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">04:30 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 32 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Cadus</h3>
                                                    <p>Esta Bodega boutique nos invita a descubrir el vino icono, que busca expresar las singulares características de los más virtuosos terrier
                                                        mendocinos.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Santiago Mayorga</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box4|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 32 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 32 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Nieto+Senetiner&from=2020-09-26+21%3A30%3A00&to=2020-09-26+22%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Nieto+Senetiner')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/lapostolle.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">05:30 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 33 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Lapostolle Cabernet Sauvignon</h3>
                                                    <p>La Bodega Lapostolle de la mano de Charles De Bournet nos invita a conocer sus vinos más emblemáticos, de gran estructura, con taninos suaves,
                                                        aterciopelados y un largo final.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Charles de Bournet</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box6|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 33 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 33 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Lapostolle&from=2020-09-26+22%3A30%3A00&to=2020-09-26+23%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Lapostolle')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/dona-paula.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">06:30 p.m.</span>
                                            @foreach($catas as $cata)
                                                @if($cata->id === 41 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Doña Paula</h3>
                                                    <p>La Bodega Doña Paula nos invita a descubrir una selección única de vinos, donde los máximos esfuerzos enológicos de la bodega y los análisis
                                                        estratégicos de terroirs, dan como resultado vinos grandiosos.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Martin Kayser</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @hasanyrole("box2|vip|catador|super-admin")

                                            @foreach($catas as $cata)
                                                @if($cata->id === 41 && (\Jenssegers\Date\Date::now() >= $cata->start_at && \Jenssegers\Date\Date::now() <= $cata->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.cata-vino.details", $cata->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($cata->id === 41 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Doña+Paula&from=2020-09-26+23%3A30%3A00&to=2020-09-27+00%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Doña+Paula')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @endhasanyrole
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
