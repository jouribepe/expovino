@extends("layouts.app")

@section("content")
    <main id="main" class="hablemos">
    <!-- <section class="breadcrumb">
            <div class="content">
                <p>
                    <a href="{{ url()->previous() }}"><img src="{{ asset("images/back.png") }}" alt> Regresar</a>
                </p>
            </div>
        </section> -->

        <section class="hablemos-content">
            <div class="content">
                <div class="title">
                    <img src="{{ asset("images/hablemos-page.png") }}" alt>
                    <h2>HablemosVino</h2>
                </div>
                <div class="bajada">
                    <h3>CRONOGRAMA DE EVENTOS</h3>
                    <p>Conoce el cronograma de eventos de HablemosVino que tenemos preparado para ti</p>
                </div>
                <div class="hablemos-cronograma">
                    <div class="content">
                        <div class="crono-table">
                            <div class="table-tabs">
                                <ul class="tabs">
                                    <li class="tab">
                                        <a href="#diaUno">Jueves 24</a>
                                    </li>
                                    <li class="tab">
                                        <a class="active" href="#diaDos">Viernes 25</a>
                                    </li>
                                    <li class="tab">
                                        <a href="#diaTres">Sábado 26</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="diaUno" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/chivas-regal.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">08:00 p.m.</span>
                                            @foreach($hablemos as $habla)
                                                @if($habla->id === 34 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Chivas Regal</h3>
                                                    <p>Pernod Ricard nos invita a descubrir uno de sus whiskys más emblemáticos junto a su embajador Jorge Ode.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Jorge Ode</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @foreach($hablemos as $habla)

                                                @if($habla->id === 34 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($habla->id === 34 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Chivas+Regal&from=2020-09-25+01%3A00%3A00&to=2020-09-25+02%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Chivas+Regal')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="diaDos" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/johnnie-walker.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">08:00 p.m.</span>
                                            @foreach($hablemos as $habla)
                                                @if($habla->id === 35 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Johnnie Walker</h3>
                                                    <p>Diageo nos trae la cocktelería a casa a través de su nueva plataforma #LetsCocktailAtHome</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Joel Chirinos - Brand Embassador</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @foreach($hablemos as $habla)

                                                @if($habla->id === 35 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($habla->id === 35 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Chivas+Regal&from=2020-09-26+01%3A00%3A00&to=2020-09-26+02%3A00%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Johnnie+Walker')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="diaTres" class="table-content">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/lagarde.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">02:30 p.m.</span>
                                            @foreach($hablemos as $habla)
                                                @if($habla->id === 36 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Lagarde</h3>
                                                    <p>La Bodega Lagarde nos invita a descubrir vinos de alta calidad elaborados en la zona vitivinícola más importante de Argentina.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Juan Roby</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @foreach($hablemos as $habla)

                                                @if($habla->id === 36 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($habla->id === 36 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Lagarde&from=2020-09-26+19%3A30%3A00&to=2020-09-26+20%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Lagarde')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/Cartavio.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">04:30 p.m.</span>
                                            @foreach($hablemos as $habla)
                                                @if($habla->id === 37 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Cartavio</h3>
                                                    <p>Cartavio nos invita a conocer características de la zona Shangri-lá cañero del mundo y sus secretos en los procesos para obtener un buen ron, así
                                                        como una receta novedosa de cocteles.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: David Romero</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @foreach($hablemos as $habla)

                                                @if($habla->id === 37 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($habla->id === 37 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Cartavio&from=2020-09-26+21%3A30%3A00&to=2020-09-26+22%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Cartavio')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/ernesto-catena-vineyards.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">06:30 p.m.</span>
                                            @foreach($hablemos as $habla)
                                                @if($habla->id === 38 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Ernesto Catena Vineyards – Wine is art</h3>
                                                    <p>La Bodega Ernesto Catena Vineyards nos enseña cómo transformar la cultura vitivinícola en un verdadero arte.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Alejandro Kuschnaroff</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @foreach($hablemos as $habla)

                                                @if($habla->id === 38 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($habla->id === 38 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Ernesto+Catena+Vineyards&from=2020-09-26+23%3A30%3A00&to=2020-09-27+00%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Wine+Is+Art')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/tabernero.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">08:30 p.m.</span>
                                            @foreach($hablemos as $habla)
                                                @if($habla->id === 39 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Vittoria - Tabernero</h3>
                                                    <p>Tabernero nos presenta su línea de vinos Vittoria donde destacan los aromas a higos y damascos secos, mucha vainilla, los cuales se refuerzan en
                                                        boca con sabores a guindones y caramelo.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Bertrand Jolly</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @foreach($hablemos as $habla)

                                                @if($habla->id === 39 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($habla->id === 39 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Tabernero&from=2020-09-27+01%3A30%3A00&to=2020-09-27+02%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Tabernero')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset("brands/vina-cobos.jpg") }}" alt>
                                        </td>
                                        <td>
                                            <span class="hora">08:30 p.m.</span>
                                            @foreach($hablemos as $habla)
                                                @if($habla->id === 40 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div id="tag-live" class="tag-live">
                                                        <span><span class="dot"></span> EN VIVO</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="info-evento">
                                                <div class="evento">
                                                    <h3>Viña Cobos</h3>
                                                    <p>La Bodega Viña Cobos nos invita a descubrir un gran vino de Argentina, un tinto que está al nivel de los más reconocidos del mundo.</p>
                                                </div>
                                                <div class="expositor">
                                                    <h4>Expositor: Andres Vignoni</h4>
                                                    <p></p>
                                                </div>
                                            </div>
                                            @foreach($hablemos as $habla)

                                                @if($habla->id === 40 && (\Jenssegers\Date\Date::now() >= $habla->start_at && \Jenssegers\Date\Date::now() <= $habla->end_at) )
                                                    <div class="cta-btn">
                                                        <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}" class="now">Ver transmisión</a>
                                                    </div>
                                                @elseif($habla->id === 40 )
                                                    <div class="cta-btn">
                                                        <a href="javascript:void(0);"
                                                           onclick="showModal('modal-cronograma', '/export/calendar?name=Viña+Cobos&from=2020-09-27+01%3A30%3A00&to=2020-09-27+02%3A30%3A00&address=Expovino&contact_email={{auth()->user()->email}}&description=Viña+Cobos')"
                                                           class="soon">Agendar</a>
                                                    </div>
                                                @endif

                                            @endforeach
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
