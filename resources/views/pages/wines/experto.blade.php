@extends("layouts.app")

@section("content")
    <main id="main" class="experto">

        <section class="experto-content">
            <div class="content">
                <div class="title">
                    <img src="{{ asset("images/experto-page.png") }}" alt>
                    <h2>ExpertoVino</h2>
                </div>
                <div class="bajada">
                    <h3>Contenido Exclusivo</h3>
                    <p>Accede a la cata privada para clientes seleccionados</p>
                </div>

                <div class="experto-detalle">

                    @livewire('expert.schedule')

                </div>
            </div>
        </section>
    </main>
@endsection
