@extends("layouts.app")

@section("content")
    <main id="main" class="viva">
        <!-- <section class="breadcrumb">
            <div class="content">
                <p>
                    <a href="{{ url()->previous() }}"><img src="{{ asset("images/back.jpg") }}" alt> Regresar</a>
                </p>
            </div>
        </section> -->

        <section class="viva-content">
            <div class="content">
                <div class="title">
                    <img src="{{ asset("images/viva_page.png") }}" alt>
                    <h2>Viva el Vino</h2>
                </div>
                <div class="bajada">
                    <h3>galería de videos</h3>
                    <p>Disfruta del contenido seleccionado para ti</p>
                </div>
                <div class="viva-grilla">
                    <div class="content">
                        <div class="crono-table">
                            <div class="table-tabs">
                                <ul class="tabs">
                                    <li class="tab">
                                        <a class="active" href="#bodegas">Nuestras Bodegas</a>
                                    </li>
                                    <li class="tab">
                                        <a href="#licores">Nuestros Licores y Cocktails</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="bodegas" class="table-content">
                                <div class="content-videos">
                                    <div class="item-viva">
                                        <div class="show yt1">
                                            <img src="{{ asset("brands/lagarde.jpg") }}" alt>
                                            <h2>Lagarde</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt2">
                                            <img src="{{ asset("brands/finca-flichman.jpg") }}" alt>
                                            <h2>Finca Flichman</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt3">
                                            <img src="{{ asset("brands/ernesto-catena-vineyards.jpg") }}" alt>
                                            <h2>Ernesto Catena Vineyards</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt4">
                                            <img src="{{ asset("brands/nieto-senetiner.jpg") }}" alt>
                                            <h2>Nieto Senetiner</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt5">
                                            <img src="{{ asset("brands/rutini-wines.jpg") }}" alt>
                                            <h2>Rutini Wines</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt6">
                                            <img src="{{ asset("brands/pago-la-jaraba.jpg") }}" alt>
                                            <h2>Pago La Jaraba</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt7">
                                            <img src="{{ asset("brands/dominio-de-punctum.jpg") }}" alt>
                                            <h2>Dominio de Punctum</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt8">
                                            <img src="{{ asset("brands/viu-manent.jpg") }}" alt>
                                            <h2>Viu Manent</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt9">
                                            <img src="{{ asset("brands/vina-cobos.jpg") }}" alt>
                                            <h2>Viña Cobos</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt10">
                                            <img src="{{ asset("brands/navarro-correas.jpg") }}" alt>
                                            <h2>Navarro Correas</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt11">
                                            <img src="{{ asset("brands/septima.jpg") }}" alt>
                                            <h2>Septima</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt12">
                                            <img src="{{ asset("brands/trapiche.jpg") }}" alt>
                                            <h2>Trapiche</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt13">
                                            <img src="{{ asset("brands/protos.jpg") }}" alt>
                                            <h2>Protos</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt14">
                                            <img src="{{ asset("brands/codorniu.jpg") }}" alt>
                                            <h2>Codorniu</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt15">
                                            <img src="{{ asset("brands/chivite.jpg") }}" alt>
                                            <h2>Chivite</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt16">
                                            <img src="{{ asset("brands/vina-carmen.jpg") }}" alt>
                                            <h2>Viña Carmen</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt17">
                                            <img src="{{ asset("brands/cicchitti.jpg") }}" alt>
                                            <h2>Cicchitti</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt18">
                                            <img src="{{ asset("brands/lorca.jpg") }}" alt>
                                            <h2>Lorca</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt19">
                                            <img src="{{ asset("brands/foster.jpg") }}" alt>
                                            <h2>Foster</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt20">
                                            <img src="{{ asset("brands/luigi-bosca.jpg") }}" alt>
                                            <h2>Luigi Bosca</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt21">
                                            <img src="{{ asset("brands/la-linda.jpg") }}" alt>
                                            <h2>La Linda</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt22">
                                            <img src="{{ asset("brands/escorihuela-gascon.jpg") }}" alt>
                                            <h2>Escorihuela Gascón</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt23">
                                            <img src="{{ asset("brands/humberto-canale.jpg") }}" alt>
                                            <h2>Humberto Canale</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt24">
                                            <img src="{{ asset("brands/dona-paula.jpg") }}" alt>
                                            <h2>Doña Paula</h2>
                                        </div>
                                    </div>
                                    <div class="item-viva">
                                        <div class="show yt25">
                                            <img src="{{ asset("brands/tabernero.jpg") }}" alt>
                                            <h2>Tabernero</h2>
                                        </div>
                                    </div>
                                    <div id="loadMoreB" class="bodegas">
                                        <a href="#">Ver más</a>
                                    </div>
                                </div>
                            </div>
                            <div id="licores" class="table-content">
                                <div class="content-videos">
                                    <div class="item-licores">
                                        <div class="show yt26">
                                            <img src="{{ asset("brands/tabernero.jpg" ) }}" alt="Tabernero">
                                            <h2>Tabernero</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt27">
                                            <img src="{{ asset("brands/carupano.jpg") }}" alt="Carúpano">
                                            <h2>Carúpano</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt28">
                                            <img src="{{ asset("brands/vinas-queirolo.jpg") }}" alt="Viñas Queirolo">
                                            <h2>Viñas Queirolo</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt29">
                                            <img src="{{ asset("brands/montesierpe.jpg") }}" alt="Montesierpe">
                                            <h2>Montesierpe</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt30">
                                            <img src="{{ asset("brands/Bombay-Sapphire.jpg") }}" alt="Bombay">
                                            <h2>Bombay</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt31">
                                            <img src="{{ asset("brands/cuatro-gallos.jpg") }}" alt="Cuatro Gallos">
                                            <h2>Cuatro Gallos</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt32">
                                            <img src="{{ asset("brands/chivas-regal.jpg") }}" alt="Chivas Regal">
                                            <h2>Chivas Regal</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt33">
                                            <img src="{{ asset("brands/johnnie-walker.jpg") }}" alt="Johnnie Walker">
                                            <h2>Johnnie Walker</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt34">
                                            <img src="{{ asset("brands/go-barman.jpg") }}" alt="Go Barman">
                                            <h2>Go Barman</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt35">
                                            <img src="{{ asset("brands/mr-perkins.jpg") }}" alt="Mr Perkins">
                                            <h2>Mr Perkins</h2>
                                        </div>
                                    </div>
                                    <div class="item-licores">
                                        <div class="show yt36">
                                            <img src="{{ asset("brands/14-inkas.jpg") }}" alt="14 Inkas">
                                            <h2>14 Inkas</h2>
                                        </div>
                                    </div>
                                    <div id="loadMoreL" class="licores">
                                        <a href="#">Ver más</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        <!-- Bodegas -->
        <div id="lightbox" class="lightbox lb1" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player1" class="player" width="560" height="315" src="https://www.youtube.com/embed/S56eyHuQEYA" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb2" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player2" class="player" width="560" height="315" src="https://www.youtube.com/embed/PJsHesMmBvE" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb3" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player3" class="player" width="560" height="315" src="https://www.youtube.com/embed/P6TeiRPjKHE" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb4" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player4" class="player" width="560" height="315" src="https://www.youtube.com/embed/t9-tYYUhP1c" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb5" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player5" class="player" width="560" height="315" src="https://www.youtube.com/embed/k2myIKrxoVQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb6" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player6" class="player" width="560" height="315" src="https://www.youtube.com/embed/2PfUTmvOmrI" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb7" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player7" class="player" width="560" height="315" src="https://www.youtube.com/embed/Xh9vltqAdxc" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb8" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player8" class="player" width="560" height="315" src="https://www.youtube.com/embed/Ftl31B-mBqQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb9" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player9" class="player" width="560" height="315" src="https://www.youtube.com/embed/UVhBAs4EXLw" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb10" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player10" class="player" width="560" height="315" src="https://www.youtube.com/embed/sHgsvr0_0D4" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb11" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player11" class="player" width="560" height="315" src="https://www.youtube.com/embed/axURiTjfTUg" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb12" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player12" class="player" width="560" height="315" src="https://www.youtube.com/embed/6o2ZATyaIfE" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb13" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player13" class="player" width="560" height="315" src="https://www.youtube.com/embed/IwlA2yiKc5A" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb14" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player14" class="player" width="560" height="315" src="https://www.youtube.com/embed/7dD2xO0gmTQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb15" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player15" class="player" width="560" height="315" src="https://www.youtube.com/embed/4dg5r5qtBGc" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb16" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player16" class="player" width="560" height="315" src="https://www.youtube.com/embed/62ZuIazoEfY" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb17" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player17" class="player" width="560" height="315" src="https://www.youtube.com/embed/edOzzjUnktI" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb18" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player18" class="player" width="560" height="315" src="https://www.youtube.com/embed/s2YUPpaPruU" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb19" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player19" class="player" width="560" height="315" src="https://www.youtube.com/embed/BTNALyrr00E" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb20" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player20" class="player" width="560" height="315" src="https://www.youtube.com/embed/p6jxPP96twI" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb21" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player21" class="player" width="560" height="315" src="https://www.youtube.com/embed/YQSoZokSyIQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb22" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player22" class="player" width="560" height="315" src="https://www.youtube.com/embed/tCKlye5MVow" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb23" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player23" class="player" width="560" height="315" src="https://www.youtube.com/embed/IR4Zoa6dp0w" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb24" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player24" class="player" width="560" height="315" src="https://www.youtube.com/embed/HJdrntsUJWs" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb25" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player25" class="player" width="560" height="315" src="https://www.youtube.com/embed/AFS8QcF9SQE" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <!-- Licores -->
        <div id="lightbox" class="lightbox lb26" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player26" class="player" width="560" height="315" src="https://www.youtube.com/embed/AFS8QcF9SQE" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb27" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player27" class="player" width="560" height="315" src="https://www.youtube.com/embed/RibeZCC7808" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb28" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player28" class="player" width="560" height="315" src="https://www.youtube.com/embed/PphRP9mktVY" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb29" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player29" class="player" width="560" height="315" src="https://www.youtube.com/embed/aKcBSHqUP_E" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb30" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player30" class="player" width="560" height="315" src="https://www.youtube.com/embed/3whiw8B4Oo0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb31" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player31" class="player" width="560" height="315" src="https://www.youtube.com/embed/ODkN1nyzX4Y" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb32" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player32" class="player" width="560" height="315" src="https://www.youtube.com/embed/dwcqywcpC6Y" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb33" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player33" class="player" width="560" height="315" src="https://www.youtube.com/embed/BYtVGV984" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb34" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player34" class="player" width="560" height="315" src="https://www.youtube.com/embed/DPkAmeYcRWQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb35" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player35" class="player" width="560" height="315" src="https://www.youtube.com/embed/oUdFcQFloCQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="lightbox" class="lightbox lb36" style="display:none">
            <div class="white_content">
                <a href="javascript:;" id="close">
                    <img src="{{ asset("images/close-dorado.png") }}" alt>
                </a>
                <div>
                    <iframe id="player36" class="player" width="560" height="315" src="https://www.youtube.com/embed/KpV_f0f86ew" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        
    </main>
@endsection