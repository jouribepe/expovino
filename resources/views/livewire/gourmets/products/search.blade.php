<div>
    <section class="productos-bodega">
        <div class="content">
            <div class="filtros">
                <div class="content">

                    @if((new \Jenssegers\Agent\Agent())->isDesktop())
                        <div class="otros-filtros solo-desktop">
                            <label>filtrar por:</label>
                            <div class="control-filter">
                                <label for="precio" class="hide"></label>
                                <select wire:model="filter.precio" name="precio" id="precio" class="precio">
                                    <option value="">Precio</option>
                                    <option value="asc">de menor a mayor</option>
                                    <option value="desc">de mayor a menor</option>
                                </select>
                            </div>
                            <div class="control-filter">
                                <div class="check-custom">
                                    <input type="checkbox" wire:model="filter.exclusivo" id="excluisividad">
                                    <label for="excluisividad">Exclusivos</label>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="solo-mobile abrir-filtros">
                        <a href="javascript:void(0)" class="mostrar">
                            <img src="{{ asset('images/filtro.png') }}" alt="">
                            filtrar
                        </a>
                    </div>
                </div>
            </div>
            <div class="vinos-bodega">
                <div id="content-gourmets-bodega" class="content-vinos-bodega gastronomia">
                    @foreach($products as $product)
                        <div class="item">
                            <div class="imagen">
                                <a href="{{ route("gourmets.product.show", $product->slug) }}"
                                   class="img-vino">
                                    <img src="{{ asset("products/{$product->image}")  }}" alt="{{ $product->name }}">
                                </a>
                            </div>
                            <div class="descripcion-vino">
                                <h3>{{ $product->name }}</h3>
                                <div class="price">
                                    <p>
                                        <span>S/ <span class="monto">{{ number_format($product->price, 2) }}</span></span>
                                    </p>
                                    @if($product->price !== $product->price_regular)
                                        <p class="regular-price">Precio regular: <span>S/ {{ number_format($product->price_regular, 2) }}</span></p>
                                    @endif
                                </div>

                                <!-- <div class="cta-btn">
                                    <a href="javascript:hideNotification('pr{{$product->sku}}')"
                                       wire:click.debounce.350ms="addProduct({{ $product->id }})">
                                        añadir al carrito
                                    </a>
                                </div> -->

                                <a href="{{ route("gourmets.product.show", $product->slug) }}">
                                    <img src="{{ asset("images/conoce_mas.png") }}" alt> Conoce más
                                </a>

                                <span id="pr{{$product->sku}}" class="item-agregado hide">
                                <img src="{{ asset('images/check_item.png') }}" alt>
                                {{ Cart::content()->where('id', $product->sku)->first() ? Cart::content()->where('id', $product->sku)->first()->qty + 1  :  1 }}
                                en el carrito
                            </span>
                            </div>
                        </div>
                    @endforeach
                </div>

                @if($products->hasPages())
                    <div class="onload-more">
                        <span style="cursor: pointer;" wire:click="$emit('load-more-product-gourmet')">ver más</span>
                    </div>
                @endif
            </div>
        </div>

        <script>
            function hideNotification(sku) {
                document.getElementById(sku).classList.remove('hide')
            }
        </script>
    </section>

    @if((new \Jenssegers\Agent\Agent())->isPhone())
        <section class="filtros-mobile solo-mobile">
            <div class="content">
                <div class="cabecera">
                    <p>Filtros</p>
                    <a href="javascript:void(0)" class="ocultar">
                        <img src="{{asset("images/close.png")}}" alt="">
                    </a>
                </div>
                <div class="filtros">
                    <div class="control-filter">
                        <label for="precio" class="hide"></label>
                        <select wire:model.defer="filter.precio" name="precio" id="precio" class="precio">
                            <option value="">Precio</option>
                            <option value="asc">de menor a mayor</option>
                            <option value="desc">de mayor a menor</option>
                        </select>
                    </div>
                    <div class="exclusividad">
                        <label>
                            <input wire:model.defer="filter.exclusivo" type="checkbox" name="exclusividad" id="excluisividad" class="filled-in"/>
                            <span>Exclusivos</span>
                        </label>
                    </div>

                    <!-- <div class="control-filter">
                        <div class="check-custom">
                            <input type="checkbox" wire:model.defer="filter.exclusivo" id="excluisividad">
                            <label for="excluisividad">Exclusivos</label>
                        </div>
                    </div> -->

                    <div class="cta-btn">
                        <a id="buscar" href="javascript:void(0)" wire:click="render" class="filtrar">Buscar</a>
                        <a href="javascript:void(0)" wire:click="$emit('reset-filters-gourmet-brand')" class="limpiar">Limpiar filtros</a>
                    </div>
                </div>
            </div>
        </section>
    @endif
</div>
