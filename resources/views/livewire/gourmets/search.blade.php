<div>
    <div class="content">
        <h2>Mundo Gourmet</h2>
        <p>Encuentra el acompañamiento perfecto para tus vinos en nuestra sección Gourmet.</p>
        <div class="filtros">
            <div class="content">

                <div class="filtro-ppal">
                    <label for="tipoComida">Tipo:</label>
                    <select wire:model="filter.tipo" name="tipoComida" id="tipoComida">
                        @foreach($productTypes as $slug => $name)
                            <option value="{{ $slug }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>

                @if((new \Jenssegers\Agent\Agent())->isDesktop())
                <div class="otros-filtros solo-desktop">
                    <label>filtrar por:</label>
                    <div class="control-filter">
                        <label for="pais" class="hide"></label>
                        <select wire:model="filter.pais" id="pais" class="pais">
                            <option value="">País</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->slug }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="control-filter">
                        <label for="precio" class="hide"></label>
                        <select wire:model="filter.precio" name="precio" id="precio" class="precio">
                            <option value="">Precio</option>
                            <option value="asc">de menor a mayor</option>
                            <option value="desc">de mayor a menor</option>
                        </select>
                    </div>
                    <div class="control-filter">
                        <div class="check-custom">
                            <input type="checkbox" wire:model="filter.exclusivo" id="excluisividad">
                            <label for="excluisividad">Exclusivos</label>
                        </div>
                    </div>
                </div>
                @endif

                <div class="solo-mobile abrir-filtros">
                    <a href="javascript:void(0)" class="mostrar">
                        <img src="{{ asset('images/filtro.png') }}" alt="">
                        filtrar
                    </a>
                </div>
            </div>
        </div>
        <div class="resultados">
            <div id="content-result-gourmet" class="content-result">
                @foreach($brands as $brand)
                    <div class="item">
                        <a href="{{ route('gourmets.show', $brand->slug) }}" class="item-link">
                            <img src="{{ asset("brands/{$brand->logo}") }}" alt="{{ $brand->name }}">
                            <h3>{{ $brand->name }}
                                <br>
                                <p class="country solo-mobile">
                                    <img src="{{ asset("images/{$brand->country->flag}") }}"
                                         alt="{{ $brand->country->name }}">
                                    {{ $brand->country->name }}
                                </p>
                            </h3>
                            <p>{{ Str::limit($brand->excerpt, 100, "...") }}</p>
                            <p class="country solo-desktop">
                                <img src="{{ asset("images/{$brand->country->flag}") }}" alt="{{ $brand->country->flag }}">
                                {{ $brand->country->name }}
                            </p>
                            <span>
                                <img src="{{ asset('images/conoce_mas.png') }}" alt=""> Conoce más
                            </span>
                        </a>
                    </div>
                @endforeach
            </div>

            @if($brands->hasPages())
                <div class="onload-more">
                    <span style="cursor: pointer;" wire:click="$emit('load-more-gourmet')">ver más</span>
                </div>
            @endif
        </div>
    </div>

    @if((new \Jenssegers\Agent\Agent())->isPhone())
    <section class="filtros-mobile solo-mobile">
        <div class="content">
            <div class="cabecera">
                <p>Filtros</p>
                <a href="javascript:void(0)" class="ocultar">
                    <img src="{{ asset('images/close.png') }}" alt="">
                </a>
            </div>
            <div class="filtros">
                <!-- <div class="filtro-ppal control-filter">
                    <label for="tipoComida" style="display: none;">Tipo:</label>
                    <select wire:model.defer="filter.tipo" name="tipoComida" id="tipoComida">
                        <option value="">Tipo</option>
                        @foreach($productTypes as $slug => $name)
                            <option value="{{ $slug }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div> -->

                <div class="control-filter">
                    <label for="pais" style="display: none;"></label>
                    <select wire:model.defer="filter.pais" id="pais" class="pais">
                        <option value="">País</option>
                        @foreach($countries as $country)
                            <option value="{{ $country->slug }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="control-filter">
                    <label for="precio" class="hide"></label>
                    <select wire:model.defer="filter.precio" name="precio" id="precio" class="precio">
                        <option value="">Precio</option>
                        <option value="asc">de menor a mayor</option>
                        <option value="desc">de mayor a menor</option>
                    </select>
                </div>
                <div class="exclusividad">
                    <label>
                        <input wire:model.defer="filter.exclusivo" type="checkbox" name="exclusividad" id="excluisividad" class="filled-in"/>
                        <span>Exclusivos</span>
                    </label>
                </div>

                <!-- <div class="control-filter">
                    <div class="check-custom">
                        <input type="checkbox" wire:model.defer="filter.exclusivo" id="excluisividad">
                        <label for="excluisividad">Exclusivos</label>
                    </div>
                </div> -->

                <div class="cta-btn">
                    <a id="buscar" href="javascript:void(0)" wire:click="render" class="filtrar">Buscar</a>
                    <a href="javascript:void(0)" wire:click="$emit('reset-filters-gourmet')" class="limpiar">Limpiar filtros</a>
                </div>
            </div>
        </div>
    </section>
    @endif
</div>
