<div>
    <section class="productos-bodega">
        <div class="content">
            <div class="filtros">
                <div class="content">
                    @if((new \Jenssegers\Agent\Agent())->isDesktop())
                        <div class="otros-filtros solo-desktop">
                            <label>filtrar por:</label>
                            <div class="control-filter">
                                <label for="tipoVino" style="display: none;"></label>
                                @if($showType)
                                    <select wire:model="filter.tipo" id="tipoVino" class="tipo-vino"
                                            style="display: block;">
                                        <option value="">Tipo</option>
                                        @foreach($wineTypes as $slug => $name)
                                            <option value="{{ $slug }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="control-filter">
                                <label for="precio" style="display: none;"></label>
                                <select wire:model="filter.precio" id="precio" class="precio">
                                    <option value="">Precio</option>
                                    <option value="asc">De menor a mayor</option>
                                    <option value="desc">De mayor a menor</option>
                                </select>
                            </div>
                            <div class="control-filter">
                                <div class="check-custom">
                                    <input type="checkbox" wire:model="filter.exclusivo" id="excluisividad">
                                    <label for="excluisividad">Exclusivos</label>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="solo-mobile abrir-filtros">
                        <a href="javascript:void(0)" class="mostrar">
                            <img src="{{ asset("images/filtro.png") }}" alt>
                            filtrar
                        </a>
                    </div>
                </div>
            </div>
            <div class="vinos-bodega">
                <div id="content-vinos-bodega" class="content-vinos-bodega">
                    @foreach($products as $product)
                        <div class="item" wire:key="product-item-{{ $product->sku }}">
                            <div class="imagen">
                                <a href="{{ route("productos.show", $product->slug) }}" class="img-vino">
                                    <img src="{{ asset("products/{$product->image}")  }}" alt="{{ $product->name }}">
                                </a>
                            </div>
                            <div class="descripcion-vino">
                                <h3>{{ $product->name }}</h3>
                                <div class="price">
                                    <p>
                                        <span>S/
                                            <span class="monto">{{ number_format($product->price, 2) }}</span>
                                        </span>
                                    </p>
                                    @if($product->price !== $product->price_regular)
                                        <p class="regular-price">Precio regular: <span>S/ {{ number_format($product->price_regular, 2) }}</span></p>
                                    @endif
                                </div>

                                <!-- <div class="cta-btn">
                                    <a href="javascript:hideNotification('pr{{$product->sku}}')"
                                       wire:click="addProduct({{ $product->id }})"
                                       wire:loading.attr="disabled">
                                        añadir al carrito
                                    </a>
                                </div> -->

                                <a href="{{ route("productos.show", $product->slug) }}">
                                    <img src="{{ asset("images/conoce_mas.png") }}" alt> Conoce más
                                </a>

                                <span id="pr{{$product->sku}}" class="item-agregado hide">
                                    <img src="{{ asset('images/check_item.png') }}" alt>
                                    +1 en el carrito
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            {{--            @if($products->hasPages())--}}
            {{--                <div class="onload-more">--}}
            {{--                    <span style="cursor: pointer" wire:click="$emit('load-more-product')">ver más</span>--}}
            {{--                </div>--}}
            {{--            @endif--}}
        </div>

        <script>
            function hideNotification(sku) {
                document.getElementById(sku).classList.remove('hide')
            }
        </script>
    </section>

    @if((new \Jenssegers\Agent\Agent())->isMobile())
        <section class="filtros-mobile solo-mobile">
            <div class="content">
                <div class="cabecera">
                    <p>Filtros</p>
                    <a href="javascript:void(0)" class="ocultar">
                        <img src="{{ asset("images/close.png") }}" alt="">
                    </a>
                </div>
                <div class="filtros">
                    <div class="exclusividad">
                        <label>
                            <input wire:model.defer="filter.exclusivo" type="checkbox" name="exclusividad"
                                   class="filled-in"/>
                            <span>Exclusivos</span>
                        </label>
                    </div>
                    @if($showType)
                        <div class="control-filter">
                            <label for="tipoVino" style="display: none;"></label>
                            <select wire:model.defer="filter.tipo" id="tipoVino" class="tipo-vino"
                                    style="display: block;">
                                <option value="">Tipo</option>
                                @foreach($wineTypes as $slug => $name)
                                    <option value="{{ $slug }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                    <div class="control-filter">
                        <label for="porPrecio" style="display: none;"></label>
                        <select wire:model.defer="filter.precio" id="porPrecio" class="precio">
                            <option value="">Precio</option>
                            <option value="">de menor a mayor</option>
                            <option value="">de mayor a menor</option>
                        </select>
                    </div>
                    <div class="cta-btn">
                        <a id="buscar" href="javascript:void(0)" wire:click="render" class="filtrar">Buscar</a>
                    </div>
                </div>
            </div>
        </section>
    @endif
</div>
