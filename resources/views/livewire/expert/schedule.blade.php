<div class="content">

    @if($scheduled < $box)
        <div class="paso seleccione">

            <div class="imagen">
                <img src="{{ asset("images/contenido-exclusivo.png") }}" alt>
                <p>Contenido <br><span>Exclusivo</span></p>
            </div>

            <div class="paso-content">
                <h2>Selecciona el día y hora de tu cata privada</h2>

                <div class="seleccionar-cata">
                    <div class="filtros">
                        <div class="content">
                            <div class="otros-filtros">
                                <form wire:submit.prevent="save">
                                    <div class="control-filter">
                                        <label for="dia">Día</label>
                                        <select wire:model="day" id="dia" class="dia pais">
                                            <option value="" selected>seleccione</option>
                                            @foreach($dates as $date => $webinar)
                                                <option value="{{ $date }}">{{ Jenssegers\Date\Date::parse($date)->format('l d') }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="control-filter">
                                        <label for="hora">Hora</label>
                                        <select wire:model="webinar_id" name="time" id="hora" class="hora pais">
                                            <option value="">seleccione</option>
                                            @foreach($hours as $hour => $webinars)
                                                @foreach($webinars as $webinar)
                                                    <option @if($webinar_id === $webinar->id) selected @endif value="{{ $webinar->id }}">{{ $hour }}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="cta-btn">
                                        <button type="submit">Reservar cata</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <p class="nota">Recuerda que una vez reservado el horario de la cata, este
                        no
                        podrá
                        ser modificado.</p>
                </div>

            </div>
        </div>
    @else
        <div class="paso confirma">
            <div class="imagen">
                <img src="{{ asset("images/contenido-exclusivo.png") }}" alt>
                <p>Contenido <br><span>Exclusivo</span></p>
            </div>
            <div class="paso-content">
                <h2>Tu cata privada ha sido <br>reservada</h2>

                @foreach($schedules as $appointment)
                <p>DÍA: {{ \Jenssegers\Date\Date::parse($appointment->start_at)->format('d M') }} | hora: {{ \Jenssegers\Date\Date::parse($appointment->start_at)->format('H:i') }}</p>
                @endforeach
                {{--                                    <div class="cta-btn">--}}
                {{--                                        <a href="#">Continuar</a>--}}
                {{--                                    </div>--}}
            </div>
        </div>
    @endif

    <div class="paso agendado hide">
        <div class="imagen">
            <img src="{{ asset("images/contenido-exclusivo.png") }}" alt>
            <p>Contenido <br><span>Exclusivo</span></p>
        </div>
        <div class="paso-content">
            <h2>Cata de vinos rosados, tintos jóvenes y tintos roble</h2>
            <p>Aprende como debes hacer una cata de un vino, de principio a fin Gael le Bec te
                explica la forma correcta de hacer la cata, los tres momento, gustativo, olfativo y
                visual</p>
            <div class="expositor">
                <p class="title">expositor: gael le bec
                    <span>DÍA: JUEVES 17 SET. | hora: 07:00 p.m.</span></p>
                <p>Gael le Bec sommelier francés residente en Barcelona ccuenta con 10 años de
                    experiencia en la cata de vinos.</p>
            </div>
        </div>
    </div>

    <div class="paso live" style="display: none">
        <div class="imagen">
            <img src="{{ asset("images/contenido-exclusivo.png") }}" alt>
            <p>Contenido <br><span>Exclusivo</span></p>
        </div>
        <div class="paso-content">
            <div id="tag-live" class="tag-live">
                <span><span class="dot"></span> EN VIVO</span>
            </div>
            <h2>Cata de vinos rosados, tintos jóvenes y tintos roble</h2>
            <p>Aprende como debes hacer una cata de un vino, de principio a fin Gael le Bec te
                explica la forma correcta de hacer la cata, los tres momento, gustativo, olfativo y
                visual</p>
            <div class="expositor">
                <p class="title">expositor: gael le bec
                    <span>DÍA: JUEVES 17 SET. | hora: 07:00 p.m.</span></p>
                <p>Gael le Bec sommelier francés residente en Barcelona ccuenta con 10 años de
                    experiencia en la cata de vinos.</p>
            </div>
            <div class="cta-btn">
                <a href="#">Ingresar a videollamada</a>
            </div>
        </div>
    </div>


</div>
