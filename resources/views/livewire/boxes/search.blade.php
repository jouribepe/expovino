<div>
    <section class="productos-bodega">
        <div class="content">
            <div class="filtros">
                <div class="content">
                    @if((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="otros-filtros solo-desktop">
                        <label>filtrar por:</label>
                        <div class="control-filter">
                            <label for="precio" class="hide"></label>
                            <select wire:model="precio" name="precio" id="precio" class="precio">
                                <option value="">Precio</option>
                                <option value="asc">de menor a mayor</option>
                                <option value="desc">de mayor a menor</option>
                            </select>
                        </div>
                    </div>
                    @endif

                    <div class="solo-mobile abrir-filtros">
                        <a href="javascript:void(0)" class="mostrar">
                            <img src="{{ asset('images/filtro.png') }}" alt="">
                            filtrar
                        </a>
                    </div>
                </div>
            </div>
            <div class="vinos-bodega boxes">
                <div id="content-boxes" class="content-vinos-bodega">
                    @foreach($products as $product)
                        <div class="item">
                            <div class="imagen">
                                <a href="{{ route('boxes.show', $product->slug) }}">
                                    <img src="{{ asset("products/{$product->image}") }}"
                                         alt="{{ $product->brand->name }}">
                                </a>
                            </div>
                            <div class="descripcion-vino">
                                <h3>{{ $product->name }}</h3>
                                <!-- <p>Acceso a catas virtuales.</p> -->
                                <div class="price">
                                    <p>
                                <span>S/
                                    <span class="monto">{{ number_format($product->price, 2) }}</span>
                                </span>
                                    </p>
                                </div>
                                <!-- <div class="cta-btn">
                                    <a href="javascript:hideNotification('pr{{$product->sku}}')"
                                       wire:click.debounce.300ms="addProduct({{ $product->id }})">
                                        añadir al carrito
                                    </a>
                                </div> -->
                                <a href="{{ route('boxes.show', $product->slug) }}">
                                    <img src="{{ asset('images/conoce_mas.png') }}" alt=""> Conoce más
                                </a>

                                <span id="pr{{$product->sku}}" class="item-agregado hide">
                                <img src="{{ asset('images/check_item.png') }}" alt>
                                +1 en el carrito
                        </span>
                            </div>
                        </div>
                    @endforeach
                </div>
{{--                @if($products->hasPages())--}}
{{--                    <div class="onload-more">--}}
{{--                        <span style="cursor: pointer;" wire:click="$emit('load-more-boxes')">ver más</span>--}}
{{--                    </div>--}}
{{--                @endif--}}
            </div>

            <script>
                function hideNotification(sku) {
                    document.getElementById(sku).classList.remove('hide')
                }
            </script>
        </div>
    </section>

    @if((new \Jenssegers\Agent\Agent())->isPhone())
        <section class="filtros-mobile solo-mobile">
            <div class="content">
                <div class="cabecera">
                    <p>Filtros</p>
                    <a href="javascript:void(0)" class="ocultar">
                        <img src="{{ asset('images/close.png') }}">
                    </a>
                </div>
                <div class="filtros">
                    <label>filtrar por:</label>
                    <div class="control-filter">
                        <label for="precio" class="hide"></label>
                        <select wire:model="precio" name="precio" id="precio" class="precio">
                            <option value="">Precio</option>
                            <option value="asc">de menor a mayor</option>
                            <option value="desc">de mayor a menor</option>
                        </select>
                    </div>
                    <div class="cta-btn">
                        <a id="buscar" href="javascript:void(0)" wire:click="render" class="filtrar">Buscar</a>
                    </div>
                </div>
            </div>
        </section>
    @endif
</div>
