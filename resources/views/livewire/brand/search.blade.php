<div>
    <section class="bodega-home">
        <!-- <div class="pasos-compra">
            <div class="content">
                <fieldset>
                    <legend>
                        <h2>Cómo comprar en el expovino</h2>
                    </legend>
                    <div class="content-pasos">
                        <div class="paso">
                            <div class="num">1</div>
                            <div class="paso-text">
                                <img src="{{ asset("images/paso_01.png") }}" alt="">
                                <p>Añade a tu carrito de compras tus vinos preferidos</p>
                            </div>
                        </div>
                        <div class="paso">
                            <div class="num">2</div>
                            <div class="paso-text">
                                <img src="{{ asset("images/paso_02.png") }}" alt="">
                                <p>Revisa tu carrito para validar tus productos</p>
                            </div>
                        </div>
                        <div class="paso">
                            <div class="num">3</div>
                            <div class="paso-text">
                                <img src="{{ asset("images/paso_03.png") }}" alt="">
                                <p>Serás redirigido a Wong.pe para finalizar tu compra</p>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div> -->
        <div id="resultados"></div>

        <div class="content content-bodega">
            <h2>Mundo de vinos y licores</h2>
            <p>Descubre toda la variedad de nuestra bodega con descuentos <br>exclusivos para ti.</p>

            <div class="filtros">
                <div class="content">
                    <div class="filtro-ppal">
                        <label for="tipoBebida">Tipo de bebida:</label>
                        <select wire:model="filter.tipo" id="tipoBebida">
                            @foreach($productTypes as $slug => $name)
                                <option value="{{ $slug }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>

                    @if((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="otros-filtros solo-desktop">
                        <label>filtrar por:</label>
                        <div class="control-filter buscador">
                            <label>
                                <input wire:model.debounce.500ms="search" type="text" placeholder="Bodega / Marca"
                                       style="margin: 0; width: 150px">
                            </label>
                        </div>
                        <div class="control-filter">
                            <label for="pais" style="display: none;"></label>
                            <select wire:model="filter.pais" id="pais" class="pais">
                                <option value="">País</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->slug }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($showVino)
                            <div class="control-filter">
                                <label for="tipoVino" style="display: none;"></label>
                                <select wire:model="filter.vino" id="tipoVino" class="tipo-vino">
                                    <option value="">Tipo</option>
                                    <option value="tinto">Tinto</option>
                                    <option value="blanco">Blanco</option>
                                    <option value="rosado">Rosado</option>
                                    <option value="espumante">Espumante</option>
                                </select>
                            </div>
                        @endif
                        <div class="control-filter">
                            <div class="check-custom">
                                <input type="checkbox" wire:model="filter.exclusivo" id="excluisividad">
                                <label for="excluisividad">Exclusivos</label>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="solo-mobile abrir-filtros">
                        <a href="javascript:void(0)" class="mostrar">
                            <img src="{{ asset("images/filtro.png") }}" alt>
                            filtrar
                        </a>
                    </div>
                </div>
            </div>

            @if(isset($brands))
                <div class="resultados">
                    <div id="content-result-brands" class="content-result">
                        @foreach($brands as $brand)
                            <div class="item">
                                <a href="{{ route("bodegas.show", $brand->slug ) }}" class="item-link">
                                    <img src="{{ asset("brands/{$brand->logo}") }}" alt="{{ $brand->name }}">
                                    <h3>{{ $brand->name }}
                                        <br>
                                        <p class="country solo-mobile">
                                            <img src="{{ asset("images/" . $brand->country->flag) }}"
                                                 alt="{{ $brand->country->name }}">
                                            {{ $brand->country->name }}
                                        </p>
                                    </h3>
                                    <p>{{ Str::limit($brand->excerpt, 100, "...") }}</p>
                                    <p class="country solo-desktop">
                                        <img src="{{ asset("images/" . $brand->country->flag) }}"
                                             alt="{{ $brand->country->name }}">
                                        {{ $brand->country->name }}
                                    </p>
                                    <span>
                                <img src="{{ asset("images/conoce_mas.png") }}" alt> Conoce más
                            </span>
                                </a>
                            </div>
                        @endforeach
                    </div>

                    @if($brands->hasPages())
                        <div class="onload-more">
                            <span style="cursor: pointer;" wire:click="$emit('load-more')">ver más</span>
                        </div>
                    @endif
                </div>
            @endif

        </div>

    </section>

    @if((new \Jenssegers\Agent\Agent())->isPhone())
    <section class="filtros-mobile solo-mobile">
        <div class="content">
            <div class="cabecera">
                <p>Filtros</p>
                <a href="javascript:void(0)" class="ocultar">
                    <img src="{{ asset("images/close.png") }}" alt="">
                </a>
            </div>
            <div class="filtros">
                <div class="control-filter buscador">
                    <label>
                        <label for="msearch" class="hide"></label>
                        <input id="msearch"
                               wire:model.defer="search"
                               type="text"
                               placeholder="Bodega / Marca">
                    </label>
                </div>
                <div class="exclusividad">
                    <label>
                        <input wire:model.defer="filter.exclusivo"
                               type="checkbox"
                               name="exclusividad"
                               class="filled-in"/>
                        <span>Exclusivos</span>
                    </label>
                </div>
                <div class="control-filter">
                    <label for="pais" style="display: none;"></label>
                    <select wire:model.defer="filter.pais" id="pais" class="pais">
                        <option value="">País</option>
                        @foreach($countries as $country)
                            <option value="{{ $country->slug }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
                @if($showVino)
                <div class="control-filter">
                    <label for="tipoVino" style="display: none;"></label>
                    <select wire:model.defer="filter.vino" id="tipoVino" class="tipo-vino">
                        <option value="">Tipo</option>
                        <option value="tinto">Tinto</option>
                        <option value="blanco">Blanco</option>
                        <option value="rosado">Rosado</option>
                        <option value="espumante">Espumante</option>
                    </select>
                </div>
                @endif
                <div class="control-filter">
                    <label for="porPrecio" style="display: none;"></label>
                    <select wire:model.defer="filter.precio" id="porPrecio" class="precio">
                        <option value="">Precio</option>
                        <option value="">de menor a mayor</option>
                        <option value="">de mayor a menor</option>
                    </select>
                </div>
                <div class="cta-btn">
                    <a id="buscar" href="javascript:void(0)" wire:click="render" class="filtrar">Buscar</a>
                    <a href="javascript:void(0)" wire:click="$emit('reset-filters')" class="limpiar">Limpiar filtros</a>
                </div>
            </div>
        </div>
    </section>
    @endif
</div>
