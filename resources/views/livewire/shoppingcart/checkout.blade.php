@if(Cart::count() === 0)

    <div class="carrito-vacio">

        <div class="text">
            <img src="{{ asset("images/carrito-page.png") }}" alt>
            <h3>TU CARRITO ESTÁ VACÍO</h3>
            <p>Navega por nuestras bodegas y encuentra tu vino ideal.</p>
            <div class="cta-btn">
                <a href="{{ url("/home") }}">Ir a bodegas</a>
            </div>
        </div>

    </div>

@else

    <div class="carrito-full">
        <div class="lista-productos">
            @foreach(Cart::content() as $row)
                <div class="producto">
                    <div class="imagen">
                        <img src="{{ asset("products/" . \App\Models\Product::where('shopping_sku', $row->id)->first()->image) }}" alt="">
                    </div>
                    <div class="descripcion">
                        <div class="nombre">
                            <h4>{{ $row->name }}</h4>
                            <a href="javascript:void(0)" wire:click="remove('{{ $row->rowId }}')"
                               wire:loading.attr="disabled">Eliminar producto</a>
                            <!-- Este es el botón para eliminar el producto -->
                        </div>
                        <div class="precio">
                            <p>Precio: <span>S/ <span class="monto">{{ number_format($row->price, 2)  }}</span></span></p>
                        </div>

                        <div class="cantidad">
                            <div class="quantity-control" data-quantity="">
                                <a href="javascript:void(0);" wire:click="updateMinus('{{ $row->rowId }}')"
                                   class="quantity-btn minus" data-quantity-minus=""
                                   wire:loading.attr="disabled">-</a>

                                <label>
                                    <input type="number" class="quantity-input"
                                           data-quantity-target="" value="{{ $row->qty }}"
                                           step="1" min="1" max="" name="quantity">
                                </label>

                                <a href="javascript:void(0);" wire:click="updatePlus('{{ $row->rowId }}')"
                                   class="quantity-btn plus" data-quantity-plus=""
                                   @if($row->qty === 10) disabled @endif
                                   wire:loading.attr="disabled">+</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="resumen-compra">
            <h3>Resumen de tu compra</h3>

            <div class="subtotal">
                <p>Subtotal: <span>S/ <span class="monto">{{ Cart::subtotal() }}</span></span></p>
            </div>

            <div class="cta-btn">
                <a href="{{ route("shoppingcart.store") }}" target="_blank" class="comprar">Comprar</a>
                <a href="{{ url("/home") }}" class="seguir">Seguir comprando</a>
            </div>

            <div class="note">
                <p>La compra de los productos finalizará en Wong.pe. Los precios indicados no incluyen el costo de envío, el cual se calculará una vez se finalice la compra en Wong.pe.<br>Stock sujeto
                    a disponibilidad de la tienda de despacho.<br>Se podrá agregar máximo 10 unidades por producto</p>
            </div>
        </div>

    </div>

@endif
