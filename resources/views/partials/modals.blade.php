<div id="modal-cronograma" class="agendar-evento hide">
    <div class="content">
        <a href="javascript:void(0);" id="close" onclick="hideModal('modal-cronograma')">
            <img src="{{ asset("images/close-dorado.png") }}" alt>
        </a>
        <h2>Añádelo a tu calendario</h2>
        <p>Selecciona dónde agendar tu evento seleccionado</p>
        <div class="cta-btn">
            <a id="scheduleGoogle" href=""
               class="btn-agendar gcalendar">
                <img src="{{ asset("images/gcalendar.png") }}" alt> Google Calendar
            </a>
            <a id="scheduleOutlook" href=""
               class="btn-agendar ocalendar">
                <img src="{{ asset("images/ocalendar.png") }}" alt> Outlook
            </a>
            <a id="scheduleCalendar" href=""
               class="btn-agendar icalendar">
                <img src="{{ asset("images/icalendar.png") }}" alt> iCalendar
            </a>
        </div>
    </div>
</div>
