<nav id="site-nav" @if(url()->current() !== url('/home')) class="no-tutorial" @endif role="navigation">
    <ul>
        <!-- <li>
            <a href="{{ route("shoppingcart.index") }}">
                <img src="{{ asset("images/carrito-nav.png") }}" alt>Carrito de Compras
            </a>
        </li> -->

        <li>
            <a href="{{ route("wines.viva-vino") }}">
                <img src="{{ asset("images/viva-vino-nav.png") }}" alt>Viva el vino
            </a>
        </li>

        <li>
            <a href="{{ route("wines.hablemos-vino") }}">
                <img src="{{ asset("images/hablemos-vino-nav.png") }}" alt>HablemosVino
            </a>
        </li>

        @hasanyrole("vip|box1|box2|box3|box4|box5|box6|catador|super-admin")
        <li>
            <a href="{{ route("wines.cata-vino") }}">
                <img src="{{ asset("images/cata-vino-nav.png") }}" alt>CataVino
            </a>
        </li>
        @endhasanyrole

        @hasanyrole("catador|super-admin")
        <li>
            <a href="{{ route("wines.experto-vino") }}">
                <img src="{{ asset("images/experto-vino-nav.png") }}" alt>ExpertoVino
            </a>
        </li>
        @endhasanyrole
    </ul>

    <div class="cronograma">
        <div class="opcion-crono">
            <a href="{{ route("crono") }}">cronograma</a>
        </div>

        @if(url()->current() === url('/home'))
            <div class="tutorial">
                <img src="{{ asset("images/tutorial.png") }}" alt="">
                <a id="tutorial" href="#">
                    ¿Tienes dudas?
                </a>
            </div>
        @endif
    </div>
</nav>
