@if ($paginator->hasPages())
<div class="paginador">
    <ul>
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="arrow" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span aria-hidden="true">
                    <img src="{{ asset("images/arrow_prev.png") }}" alt>
                </span>
            </li>
        @else
            <li class="arrow">
                <button type="button" wire:click="previousPage" rel="prev" aria-label="@lang('pagination.previous')">
                    <img src="{{ asset("images/arrow_prev.png") }}" alt>
                </button>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page === $paginator->currentPage())
                        <li class="number current" aria-current="page">
                            <a href="javascript:void(0)">{{ $page }}</a>
                        </li>
                    @else
                        <li class="number">
                            <button type="button" wire:click="gotoPage({{ $page }})">{{ $page }}</button>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="arrow">
                <button type="button" wire:click="nextPage" rel="next" aria-label="@lang('pagination.next')">
                    <img src="{{ asset("images/arrow_next.png") }}" alt>
                </button>
            </li>
        @else
            <li class="arrow" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span aria-hidden="true">
                    <img src="{{ asset("images/arrow_next.png") }}" alt>
                </span>
            </li>
        @endif
    </ul>
</div>
@endif
