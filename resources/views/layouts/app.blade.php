<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="shortcut icon" href="{{ asset("images/favicon.svg") }}"/>

    <meta name="description" content=""/>
    <link rel="canonical" href=""/>

    <meta property="og:locale" content="es_ES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <!-- <meta property="og:image" content="images/share.jpg"> -->

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content=""/>
    <meta name="twitter:title" content=""/>
    <meta name="twitter:site" content=""/>
    <!-- <meta name="twitter:image" content="assets/img/share.jpg" /> -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @livewireStyles

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MDF8CMJ');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MDF8CMJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<header id="masthead" role="banner">
    <div class="container">
        <div class="logo">
            <a href="{{ url("/home") }}">
                <img src="{{ asset("images/logo_expovino.png") }}" alt="{{ config("app.name") }}">
                <h1>{{ config("app.name") }}</h1>
            </a>
        </div>
        <div class="opciones-cabecera">
        <!-- <div class="tutorial">
                <a href="#">
                    <img src="{{ asset("images/tutorial.png") }}" alt="">
                </a>
            </div> -->
            <div class="welcome" style="display: flex;">
                <a id="logout" href="javascript:void(0)"
                   title="Salir" style="display: inline-block; margin-right: 10px">
                    <img src="{{ asset("images/logout.png") }}" alt="">
                </a>
                <p style="display: inline-block; margin: 0;">Hola, <strong>{{ auth()->user()->first_name }}</strong></p>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>

            <!-- @livewire('shoppingcart.cart') -->

            <button class="hamburger hamburger--boring" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
            </button>
        </div>
    </div>

    @include('partials.navigation')
</header>

<div id="app">
    @yield('content')

    <div class="label-wong">
        <a href="https://www.wong.pe/especiales/expovino-2020" target="_blank">
            <img src="{{ asset("images/wong_label.png") }}" alt>
        </a>
    </div>
</div>

<footer>
    <div class="content">
        <div class="logo-footer">
            <img src="{{ asset("images/logo_wong.png") }}" alt="Wong">
            <span>&copy; 2020 Wong, Lima - Perú</span>
        </div>
        <div class="politicas">
            <a href="#tlegales" id="tycAbrir" onclick="$('#tlegales').show(300);">Protección de datos personales</a>
        </div>
    </div>
</footer>
<div id="tlegales" style="display: none;">
    <div class="legales-content">
        <a href="javascript:void(0)" id="close">
            <i class="fas fa-times"></i>
        </a>
        <div class="legales-txt">
            <h2>Protección de Datos Personales</h2>
            <div class="content-legales">
                <h3>1. INFORMACIÓN PARA NUESTROS CLIENTES</h3>
                <p>Los datos personales que el Cliente ha facilitado para en los formularios web y documentos análogos incluidos en el presente sitio, son utilizados con la finalidad de gestionar su participación y actividades propias del evento virtual Expovino 2020 y forman parte del banco de datos denominado “Clientes” de titularidad de Cencosud Retail Perú S.A. (en adelante WONG).</p>
                <p>El Cliente puede ejercer sus derechos de acceso, actualización, inclusión, rectificación, supresión y oposición, respecto de sus datos personales en los términos previstos en la Ley N° 29733 – Ley de Protección de Datos Personales, y su Reglamento aprobado por el Decreto Supremo N° 003-2013-JUS, conforme a lo indicado en el acápite siguiente “Información para el Ejercicio de Derechos ARCO”.</p>
                <br>
                <h3>2. INFORMACIÓN PARA EL EJERCICIO DE DERECHOS ARCO</h3>
                <p>El titular de los datos personales, para ejercer sus derechos de acceso, actualización, inclusión, rectificación, supresión y oposición, o cualquier otro que la ley establezca, deberá presentar una solicitud escrita en nuestra oficina principal ubicada en Calle Augusto Angulo Nº 130 Urb. San Antonio Miraflores – Lima, conforme al “Formato Modelo para el Ejercicio de Derechos ARCO” en el horario establecido para la atención al público o mediante el envío del formato debidamente llenado al correo electrónico: <a href="mailto:servicioalclienteonline@cencosud.com.pe">servicioalclienteonline@cencosud.com.pe</a>.</p>
                <br>
                <h3>3. FORMATOS MODELO PARA EL EJERCICIO DE DERECHOS ARCO</h3>
                <p>En el siguiente enlace puede descargar los Formatos Modelo para el Ejercicio de Derechos ARCO.</p>
                <a class="descargar" href="/assets/legales/DERECHOS-ARCO.zip" download>Descargar formatos modelo</a>
            </div>
        </div>
    </div>
</div>


@include('partials.modals')

<!-- Scripts -->
@livewireScripts
<script type="text/javascript">
    function hideModal(elem) {
        document.getElementById(elem).classList.add('hide')
    }

    function showModal(elem, href) {
        $('#scheduleGoogle, #scheduleOutlook, #scheduleCalendar').attr('href', href)

        document.getElementById(elem).classList.remove('hide')
    }
</script>
<script src="{{ mix('js/app.js') }}"></script>
<script type="text/javascript">
    $('#logout').click(function (e) {
        e.preventDefault();
        $('#logout').hide();
        $('#logout-form').submit();
    });
</script>
</body>
</html>
