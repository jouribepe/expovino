<form action="{{ url('/upload') }}" method="post" enctype="multipart/form-data">
    @csrf
    <input name="file" type="file">
    <button type="submit">Upload</button>
</form>
