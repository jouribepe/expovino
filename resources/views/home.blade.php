@extends('layouts.app')

@section('content')
    <main id="main">
        <section class="eventos primero">
            <div class="content">
                <div class="slider banners-home">
                    <div class="slider-for">

                        <!-- Banners Catas -->
                        @hasanyrole("vip|box1|box2|box3|box4|box5|box6|catador|super-admin")
                        @foreach($catas as $cata)

                            <div class="item crono">
                                <picture>
                                    <source srcset="{{ asset("images/{$cata->banner}") }}" media="(min-width: 300px) and (max-width: 768px)">
                                    <img src="{{ asset("images/{$cata->banner}") }}" alt="{{ config("app.name") }}">
                                </picture>
                            </div>

                        @endforeach
                        @endhasanyrole
                    <!-- Fin Banners Catas -->

                        <!-- Banners Hablemos -->
                        @foreach($hablemos  as $habla)
                            <div class="item crono">
                                <picture>
                                    <source srcset="{{ asset("images/{$habla->banner}") }}" media="(min-width: 300px) and (max-width: 768px)">
                                    <img src="{{ asset("images/{$habla->banner}") }}" alt="{{ config("app.name") }}">
                                </picture>
                            </div>
                        @endforeach
                    <!-- Fin Banners Hablemos -->

                        <!-- Banners Expertos -->
                        {{--                        @hasanyrole("catador|super-admin")--}}
                        {{--                        <div class="item crono">--}}
                        {{--                                <picture>--}}
                        {{--                                    <source srcset="{{ asset("images/intipalka_7pm.jpg") }}"--}}
                        {{--                                            media="(min-width: 300px) and (max-width: 768px)">--}}
                        {{--                                    <img src="{{ asset("images/intipalka_7pm.jpg") }}">--}}
                        {{--                                </picture>--}}
                        {{--                            </div>--}}
                        {{--                        @endhasanyrole--}}
                    <!-- Fin Banners Expertos -->

                        {{--                       @hasanyrole("catador|super-admin")--}}
                        {{--                        <div class="item crono">--}}
                        {{--                                <picture>--}}
                        {{--                                    <source srcset="{{ asset("images/intipalka_7pm.jpg") }}"--}}
                        {{--                                            media="(min-width: 300px) and (max-width: 768px)">--}}
                        {{--                                    <img src="{{ asset("images/intipalka_7pm.jpg") }}">--}}
                        {{--                                </picture>--}}
                        {{--                            </div>--}}
                        {{--                        @endhasanyrole--}}
                    <!-- Fin Banners Expertos -->


                        <!-- Banner Risaterapia -->
                    <!-- <div class="item crono">
                            <picture>
                                <source srcset="{{ asset("images/risaterapia_2509.jpeg") }}"
                                        media="(min-width: 300px) and (max-width: 768px)">
                                <img src="{{ asset("images/risaterapia_2509.jpeg") }}" alt="{{ config("app.name") }}">
                            </picture>
                        </div> -->
                        <!-- Fin Banner Risaterapia -->

                        <div class="item crono">
                            <picture>
                                <source srcset="{{ asset("images/banner-boxes.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 768px)">
                                <img src="{{ asset("images/banner-boxes.jpg") }}" alt="{{ config("app.name") }}">
                            </picture>
                        </div>

                        <div class="item crono">
                            <picture>
                                <source srcset="{{ asset("images/gourmet-banner.jpeg") }}"
                                        media="(min-width: 300px) and (max-width: 768px)">
                                <img src="{{ asset("images/gourmet-banner.jpeg") }}" alt="{{ config("app.name") }}">
                            </picture>
                        </div>

                        <div class="item crono">
                            <picture>
                                <source srcset="{{ asset("images/banner-copas.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 768px)">
                                <img src="{{ asset("images/banner-copas.jpg") }}" alt="{{ config("app.name") }}">
                            </picture>
                        </div>

                        <div class="item crono">
                            <picture>
                                <source srcset="{{ asset("images/banner_lapractica.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 768px)">
                                <img src="{{ asset("images/banner_lapractica.jpg") }}" alt="{{ config("app.name") }}">
                            </picture>
                        </div>

                        @if($products->count() > 0)
                            @foreach($products as $product)
                                <div class="item sale-exclusive">
                                    <picture>
                                        <source srcset="{{ asset("images/banner_ofertaexclusiva.jpg") }}"
                                                media="(min-width: 300px) and (max-width: 768px)">
                                        <img src="{{ asset("images/banner_ofertaexclusiva.jpg") }}"
                                             alt="{{ config("app.name") }}">
                                    </picture>
                                    <div class="label sale-exclusive">
                                        <h2>Hora de vino</h2>
                                        <span>Descuento único</span>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>

                    <div class="slider-nav">

                        <!-- Banner Catas -->
                        @hasanyrole("vip|box1|box2|box3|box4|box5|box6|catador|super-admin")
                        @foreach($catas as $cata)
                            <div class="item">
                                <div class="description crono">
                                    <div class="content">
                                        <div class="title">
                                            <img src="{{ asset("images/la_practica.png") }}" alt="Boxes">
                                            <h2>¡Estamos en vivo!</h2>
                                        </div>
                                        <div class="text">
                                            <p>Expositor: {{ $cata->speaker }}</p>
                                        </div>
                                        <div class="cta-btn">
                                            <a href="{{ route("wines.cata-vino.details", $cata->slug) }}">Míralo aquí</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endhasanyrole
                    <!--Fin Banner Catas -->

                        <!-- Banners Hablemos -->
                        @foreach($hablemos  as $habla)
                            <div class="item">
                                <div class="description crono">
                                    <div class="content">
                                        <div class="title">
                                            <img src="{{ asset("images/la_practica.png") }}" alt="Boxes">
                                            <h2>¡Estamos en vivo!</h2>
                                        </div>
                                        <div class="text">
                                            <p>Expositor: {{ $habla->speaker }}</p>
                                        </div>
                                        <div class="cta-btn">
                                            <a href="{{ route("wines.hablemos-vino.details", $habla->slug) }}">Míralo aquí</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <!-- Fin Banners Hablemos -->

                        {{--                        @hasanyrole("catador|super-admin")--}}
                        {{--                        <div class="item">--}}
                        {{--                                <div class="description crono">--}}
                        {{--                                    <div class="content">--}}
                        {{--                                        <div class="title">--}}
                        {{--                                            <img src="{{ asset("images/la_practica.png") }}" alt="Boxes">--}}
                        {{--                                            <h2>¡Estamos en vivo!</h2>--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="text">--}}
                        {{--                                            <p>Expositor: LUIS GOMEZ</p>--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="cta-btn">--}}
                        {{--                                            <a href="https://expovino.com.pe/cata-vino/detalle/intipalka-viernes">Míralo aquí</a>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        @endhasanyrole--}}

                

                        <!-- Banner Risaterapia -->
                        <!-- <div class="item">
                            <div class="description crono">
                                <div class="content">
                                    <div class="title">
                                        <h2>RISATERAPIA</h2>
                                    </div>
                                    <div class="text">
                                        <p>Con Saskia Bernaola y Lelé Mikati</p>
                                        <p>A través de juegos de improvisación teatral, números de malabares, juegos de ritmo vocales y situaciones cómicas se logrará vencer esa barrera que no nos
                                            permite reírnos con libertad y hace que todo lo tomemos demasiado en serio.</p>
                                        <p>Media hora de puro humor y cero estrés.</p>
                                    </div>
                                    <div class="cta-btn">
                                        <a href="https://expovino.com.pe/hablemos-vino/detalle/risaterapia-viernes">Míralo aquí</a>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- Fin Banner Risaterapia -->

                        <div class="item">
                            <div class="description crono">
                                <div class="content">
                                    <div class="title">
                                        <img src="{{ asset("images/la_practica.png") }}" alt="Boxes">
                                        <h2>Boxes Expovino </h2>
                                    </div>
                                    <div class="text">
                                        <p>Compra un Box y lleva tu experiencia al siguiente nivel.</p>
                                    </div>
                                    <div class="cta-btn">
                                        <a href="{{ route("boxes.index") }}">Conoce nuestros boxes</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="description crono">
                                <div class="content">
                                    <div class="title">
                                        <img src="{{ asset("images/gourmet-nav.png") }}" alt="Boxes">
                                        <h2>Crea momentos únicos con el maridaje perfecto.</h2>
                                    </div>
                                    <div class="text">
                                        <p>Descubre toda la variedad de carnes, embutidos, quesos y fiambres que tenemos para ti.</p>
                                    </div>
                                    <div class="cta-btn">
                                        <a href="{{ route("gourmets.index") }}">Encuéntrala aquí</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="description crono">
                                <div class="content">
                                    <div class="title">
                                        <img src="{{ asset("images/la_practica.png") }}" alt="Boxes">
                                        <h2>Disfruta de un buen vino con riedel</h2>
                                    </div>
                                    <div class="text">
                                        <p>Las copas de cristal Riedel están diseñadas para adaptarse a diferentes variedades de uvas.</p>
                                    </div>
                                    <div class="cta-btn">
                                        <a href="{{ route('copas') }}">Vive la experiencia Riedel</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="description crono">
                                <div class="content">
                                    <div class="title">
                                        <img src="{{ asset("images/la_practica.png") }}" alt="Cata Vino">
                                        <h2>La fiesta del vino más grande del país</h2>
                                    </div>
                                    <div class="text">
                                        <p>Descubre todas las experiencias que hemos preparado para ti:</p>
                                        <ul>
                                            <li>Más de 50 bodegas de vinos y más de 70 marcas de licores exclusivos.</li>
                                            <li>Videos de diversas bodegas de todo el mundo.</li>
                                            <li>Webinars para expandir tus conocimientos.</li>
                                            <li>Catas virtuales de la mano de expertos.</li>
                                        </ul>
                                    </div>
                                    <div class="cta-btn">
                                        <a href="{{ route("crono") }}">Descubre nuestro cronograma</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($products->count() > 0)
                            @foreach($products as $product)
                                <div class="item">
                                    <div class="description sale-exclusive">
                                        <div class="content">
                                            <div class="tag-sale">
                                                <span>-{{ $product->discount }}<span>%</span></span>
                                            </div>
                                            <div class="title">
                                                <h2>{{ $product->name }}</h2>
                                            </div>
                                            <div class="text">
                                                <p>
                                                    <img src="{{ asset("images/{$product->brand->country->flag}") }}"
                                                         alt="España">
                                                    {{ $product->brand->country->name }}
                                                </p>
                                                <p class="bodega">Bodegas <strong>{{ $product->brand->name }}</strong>
                                                </p>
                                            </div>
                                            <div class="cta-btn">
                                                <a href="#" class="comprar">Comprar</a>
                                            </div>
                                        </div>
                                        <div class="counter">
                                            <p>Descuento válido solo por:</p>
                                            {{ $product->promotion_end_at }}
                                            <div onLoad="mostrar_hora()">
                                                <div id="fecha">
                                                    <span id="hora"></span>HR <span class="dos-pts">:</span><span
                                                        id="minuto"></span> MIN <span class="dos-pts">:</span><span
                                                        id="segundo"></span> SEG
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>

        <section class="content-users">
            <div class="content">
                <div class="access">
                    <div class="item viva-vino">
                        <a href="{{ route("wines.viva-vino") }}">
                            <img src="{{ asset("images/item_viva_elvino.png") }}" alt="Viva el vino">
                            <div class="description">
                                <span><span class="icon"></span> Viva el vino</span>
                                <p>Conoce más sobre el <strong>arte del vino</strong> con estos videos que tenemos para
                                    ti.
                                </p>
                            </div>
                        </a>
                    </div>

                    <div class="item hablemos-vino">
                        <a href="{{ route("wines.hablemos-vino") }}">
                            <img src="{{ asset("images/item_hablemos_vino.png") }}" alt="Hablemos vino">
                            <div class="description">
                                <span><span class="icon"></span> HablemosVino</span>
                                <p>Descubre los <strong>secretos del vino</strong> de la mano de expertos con nuestros
                                    webinars.</p>
                            </div>
                        </a>
                    </div>

                    @hasanyrole("vip|box1|box2|box3|box4|box5|box6|catador|super-admin")
                    <div class="item cata-vino">
                        <a href="{{ route("wines.cata-vino") }}">
                            <img src="{{ asset("images/item_cata_vino.png") }}" alt="Cata Vino">
                            <div class="description">
                                <span><span class="icon"></span> CataVino</span>
                                <p>Disfruta de nuestras <strong>catas virtuales</strong> y conviértete en un verdadero
                                    catador.</p>
                            </div>
                            {{--                            <div id="tag-live" class="tag-live">--}}
                            {{--                                <span><span class="dot"></span> EN VIVO</span>--}}
                            {{--                            </div>--}}
                        </a>
                    </div>
                    @endhasanyrole

                    @hasanyrole("catador|super-admin")
                    <div class="item experto-vino">
                        <a href="{{ route("wines.experto-vino") }}">
                            <img src="{{ asset("images/item_experto_vino.png") }}" alt="Experto vino">
                            <div class="description">
                                <span><span class="icon"></span> ExpertoVino</span>
                                <p>Comparte con <strong>nuestros sommeliers</strong> en nuestras <strong>catas
                                        privadas</strong> por invitación.</p>
                            </div>
                            <div id="sale-exclusive" class="label sale-exclusive">
                                <h2>Contenido</h2>
                                <span>Exclusivo</span>
                            </div>
                        </a>
                    </div>
                    @endhasanyrole
                </div>
            </div>
        </section>

    @livewire('brand.search')

    <!-- TODO: Este mensaje se muestra cuando ingresa a la web un cliente VIP, solo se debe mostrar la primera vez que entre luego del registro. -->
        <!-- <div class="cliente-vip">
            <div class="content">
                <h2>Hola Michel</h2>
                <p class="bajada">Como sabemos que eres un Cliente VIP, has accedido a una serie de descuentos que encontrarás aquí.</p>
                <p>Puedes participar de una Cata privada para aquellos que adquieran el pack del Expovino.</p>
                <div class="cta-btn">
                    <a href="#">Conoce más</a>
                </div>
            </div>
        </div> -->
    </main>

    <div class="tutorial-box solo-desktop">
        <div class="content">
            <div class="pasos pasoUno">
                <img src="{{ asset('images/paso_1.png') }}" alt="">
            </div>
            <div class="pasos pasoDos">
                <img src="{{ asset('images/paso_2.png') }}" alt="">
            </div>
            <div class="pasos pasoTres">
                <img src="{{ asset('images/paso_3.png') }}" alt="">
            </div>
            <div class="pasos pasoCuatro">
                <img src="{{ asset('images/paso_4.png') }}" alt="">
            </div>
            <div class="pasos pasoCinco">
                <img src="{{ asset('images/paso_5.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="tutorial-box solo-mobile">
        <div class="content">
            <div class="pasos pasoUno">
                <img src="{{ asset('images/m_paso_1.png') }}">" alt="">
            </div>
            <div class="pasos pasoDos">
                <img src="{{ asset('images/m_paso_2.png') }}">" alt="">
            </div>
            <div class="pasos pasoTres">
                <img src="{{ asset('images/m_paso_3.png') }}">" alt="">
            </div>
            <div class="pasos pasoCuatro">
                <img src="{{ asset('images/m_paso_4.png') }}">" alt="">
            </div>
            <div class="pasos pasoCinco">
                <img src="{{ asset('images/m_paso_5.png') }}">" alt="">
            </div>
        </div>
    </div>
@endsection
