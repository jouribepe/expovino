<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <!-- <link rel="shortcut icon" href="assets/img/favicon.png" /> -->

    <meta name="description" content="" />
    <link rel="canonical" href="" />

    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <!-- <meta property="og:image" content="assets/img/share.jpg"> -->

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:site" content="" />
    <!-- <meta name="twitter:image" content="assets/img/share.jpg" /> -->


	<title>Wong | Expovino en casa</title>

	<link rel="stylesheet" type="text/css" href="{{ asset("assets/css/normalize.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/css/previa.css") }}">
</head>
<body>
	<main>
		<section class="previa">
			<div class="content">
				<div class="img">
					<picture>
						<source srcset="{{ asset("assets/img/m_previa.jpg") }}" media="(min-width: 300px) and (max-width: 768px)">
						<img src="{{ asset("assets/img/d_previa.jpg") }}" alt="Wong | Expovinos en casa">
					</picture>
				</div>
				<div class="texto">
					<h2>!Gracias por acompañarnos!</h2>
					<img src="{{ asset("assets/img/logo_expovino.png") }}" alt="Wong | Expovinos en casa">
				</div>
			</div>
		</section>
	</main>
</body>
</html>
