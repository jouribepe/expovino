<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>

    <meta name="description" content=""/>
    <link rel="canonical" href=""/>

    <meta property="og:locale" content="es_ES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <!-- <meta property="og:image" content="images/share.jpg"> -->

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content=""/>
    <meta name="twitter:title" content=""/>
    <meta name="twitter:site" content=""/>
    <!-- <meta name="twitter:image" content="assets/img/share.jpg" /> -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MDF8CMJ');</script>
    <!-- End Google Tag Manager -->

</head>
<body class="login">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MDF8CMJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="content">
        <div class="logo">
            <img src="{{ asset("images/logo_expovino.png") }}" alt="Wong | Expovinos en casa">
            <h1>Wong | Expovinos en casa</h1>
        </div>
        <!-- <div class="presentacion">
            <h2>La fiesta del vino está de vuelta.</h2>
            <p class="bajada">Te invitamos a celebrar con nosotros nuestra pasión por el vino en el <br>Expovino en
                casa. Disfruta del evento de vino más grande del país <br>ahora en un nuevo formato digital.</p>
            <p class="texto">Acompáñanos y conviértete en un verdadero experto con tres días de <br>webinars, catas
                virtuales, grandes premios y los mejores descuentos en vinos.</p>
        </div> -->
    </div>
</header>

<div id="app">
    <main>
        <section class="inicio">
            <div class="content">
                <div class="slider">
                    <div class="fade">
                        <div class="item">
                            <picture>
                                <source srcset="{{ asset("images/m_comparte.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 801px)">
                                <img src="{{ asset("images/d_comparte.jpg") }}" alt="Wong | Expovinos en casa">
                            </picture>
                            <div class="descripcion">
                                <p class="title">com <br>par <br>te</p>
                                <div class="bajada">
                                    <p>La fiesta del vino más grande del país está de vuelta</p>
                                    <p>Celebremos nuestra pasión por el vino desde casa con el Expovino virtual, donde
                                        encontrarás:</p>
                                    <ul>
                                        <li>Las mejores experiencias junto a nuestros expertos.</li>
                                        <li>Más de 50 bodegas de vinos y más de 70 marcas de licores de todo el mundo.
                                        </li>
                                        <li>Exclusivos descuentos.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <picture>
                                <source srcset="{{ asset("images/m_descubre.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 801px)">
                                <img src="{{ asset("images/d_descubre.jpg") }}" alt="Wong | Expovinos en casa">
                            </picture>
                            <div class="descripcion">
                                <p class="title">des <br>cu <br>bre</p>
                                <div class="bajada">
                                    <p>La fiesta del vino más grande del país está de vuelta</p>
                                    <p>Celebremos nuestra pasión por el vino desde casa con el Expovino virtual, donde
                                        encontrarás:</p>
                                    <ul>
                                        <li>Las mejores experiencias junto a nuestros expertos.</li>
                                        <li>Más de 50 bodegas de vinos y más de 70 marcas de licores de todo el mundo.
                                        </li>
                                        <li>Exclusivos descuentos.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <picture>
                                <source srcset="{{ asset("images/m_conoce.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 801px)">
                                <img src="{{ asset("images/d_conoce.jpg") }}" alt="Wong | Expovinos en casa">
                            </picture>
                            <div class="descripcion">
                                <p class="title">co <br>no <br>ce</p>
                                <div class="bajada">
                                    <p>La fiesta del vino más grande del país está de vuelta</p>
                                    <p>Celebremos nuestra pasión por el vino desde casa con el Expovino virtual, donde
                                        encontrarás:</p>
                                    <ul>
                                        <li>Las mejores experiencias junto a nuestros expertos.</li>
                                        <li>Más de 50 bodegas de vinos y más de 70 marcas de licores de todo el mundo.
                                        </li>
                                        <li>Exclusivos descuentos.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <picture>
                                <source srcset="{{ asset("images/m_disfruta.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 801px)">
                                <img src="{{ asset("images/d_disfruta.jpg") }}" alt="Wong | Expovinos en casa">
                            </picture>
                            <div class="descripcion">
                                <p class="title">dis <br>fru <br>ta</p>
                                <div class="bajada">
                                    <p>La fiesta del vino más grande del país está de vuelta</p>
                                    <p>Celebremos nuestra pasión por el vino desde casa con el Expovino virtual, donde
                                        encontrarás:</p>
                                    <ul>
                                        <li>Las mejores experiencias junto a nuestros expertos.</li>
                                        <li>Más de 50 bodegas de vinos y más de 70 marcas de licores de todo el mundo.
                                        </li>
                                        <li>Exclusivos descuentos.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <picture>
                                <source srcset="{{ asset("images/m_siente.jpg") }}"
                                        media="(min-width: 300px) and (max-width: 801px)">
                                <img src="{{ asset("images/d_siente.jpg") }}" alt="Wong | Expovinos en casa">
                            </picture>
                            <div class="descripcion">
                                <p class="title">sien <br>te</p>
                                <div class="bajada">
                                    <p>La fiesta del vino más grande del país está de vuelta</p>
                                    <p>Celebremos nuestra pasión por el vino desde casa con el Expovino virtual, donde
                                        encontrarás:</p>
                                    <ul>
                                        <li>Las mejores experiencias junto a nuestros expertos.</li>
                                        <li>Más de 50 bodegas de vinos y más de 70 marcas de licores de todo el mundo.
                                        </li>
                                        <li>Exclusivos descuentos.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="formulario">
                    <div class="ir-otro">
                        <p>Si aún no te has <br>registrado, <strong><a href="{{ url("/register") }}">ingresa
                                    aquí</a></strong>.</p>
                        <a href="{{ url("/register") }}">Registrarme</a>
                    </div>
                    <div class="form-login inner">
                        <form id="login" class="form-register" method="POST" action="{{ route('login') }}">
                            <h3>Ingresa aquí</h3>

                            {{ csrf_field() }}

                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <label class="form-row-inner">
                                        <span class="label">E-mail</span>
                                        <input type="text" name="email" id="email" onblur="verifEmail(this)"
                                               onkeyup="verifEmail(this); ocultarErrores()" class="form-control email"
                                               required>
                                        <span class="border"></span>
                                        <span class="error" id="lbEmail" style="display: none;"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <label class="form-row-inner">
                                        <span class="label">Número de documento</span>
                                        <input type="text" class="form-control dni" id="dni" name="password"
                                               maxlength="11" onblur="verifDNI(this)"
                                               onkeyup="verifDNI(this); ocultarErrores()" required>
                                        <span class="border"></span>
                                        <span class="error" id="lbDNI" style="display: none;"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-row form-row-btn">
                                <div class="form-holder form-holder-2">
                                    <button id="btnRegistro" class="enviar " type="submit">
                                        <img src="{{ asset("images/copa_boton.png") }}" alt="">
                                        <span>Ingresar</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>

    {!! app('captcha')->render(); !!}
</div>

<footer>
    <div class="content">
        <div class="logo-footer">
            <img src="{{ asset("images/logo_wong.png") }}" alt="Wong">
            <span>&copy; 2020 Wong, Lima - Perú</span>
        </div>
        <div class="politicas">
            <a href="#tlegales" id="tycAbrir" onclick="$('#tlegales').show(300);">Protección de datos personales</a>
        </div>
    </div>
</footer>
<div id="tlegales" style="display: none;">
    <div class="legales-content">
        <a href="javascript:void(0)" id="close">
            <i class="fas fa-times"></i>
        </a>
        <div class="legales-txt">
            <h2>Protección de Datos Personales</h2>
            <div class="content-legales">
                <h3>1. INFORMACIÓN PARA NUESTROS CLIENTES</h3>
                <p>Los datos personales que el Cliente ha facilitado para en los formularios web y documentos análogos incluidos en el presente sitio, son utilizados con la finalidad de gestionar su participación y actividades propias del evento virtual Expovino 2020 y forman parte del banco de datos denominado “Clientes” de titularidad de Cencosud Retail Perú S.A. (en adelante WONG).</p>
                <p>El Cliente puede ejercer sus derechos de acceso, actualización, inclusión, rectificación, supresión y oposición, respecto de sus datos personales en los términos previstos en la Ley N° 29733 – Ley de Protección de Datos Personales, y su Reglamento aprobado por el Decreto Supremo N° 003-2013-JUS, conforme a lo indicado en el acápite siguiente “Información para el Ejercicio de Derechos ARCO”.</p>
                <br>
                <h3>2. INFORMACIÓN PARA EL EJERCICIO DE DERECHOS ARCO</h3>
                <p>El titular de los datos personales, para ejercer sus derechos de acceso, actualización, inclusión, rectificación, supresión y oposición, o cualquier otro que la ley establezca, deberá presentar una solicitud escrita en nuestra oficina principal ubicada en Calle Augusto Angulo Nº 130 Urb. San Antonio Miraflores – Lima, conforme al “Formato Modelo para el Ejercicio de Derechos ARCO” en el horario establecido para la atención al público o mediante el envío del formato debidamente llenado al correo electrónico: <a href="mailto:servicioalclienteonline@cencosud.com.pe">servicioalclienteonline@cencosud.com.pe</a>.</p>
                <br>
                <h3>3. FORMATOS MODELO PARA EL EJERCICIO DE DERECHOS ARCO</h3>
                <p>En el siguiente enlace puede descargar los Formatos Modelo para el Ejercicio de Derechos ARCO.</p>
                <a class="descargar" href="/assets/legales/DERECHOS-ARCO.zip" download>Descargar formatos modelo</a>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">

    $('#btnRegistro').click(function (e) {
        e.preventDefault()

        $("#lbEmail").hide()
        $("#lbDNI").hide()

        let $email = $("#email").val()
        let $dni = $("#dni").val()

        if ($dni === '') {
            $("#lbDNI").html('Ingrese un DNI válido').show()
            return true
        }

        if ($email === '') {
            $("#lbEmail").html('Ingrese un DNI válido').show()
            return true
        }

        //$('#btnRegistro').hide()

        const elForm = $('#login')
        const _action = elForm.attr('action')
        const _method = elForm.attr('method')
        const _formData = elForm.serialize()

        $.ajax({
            url: _action,
            type: _method,
            data: _formData,

            success: res => {
                //console.log(res)
                top.location.href = '/home'
            },

            error: res => {
                console.error(res)
                top.location.reload()
            },
        })

    })

</script>

</body>
</html>
