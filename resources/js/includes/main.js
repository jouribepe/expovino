$(document).ready(function () {
    $(function () {
        let $hamburger = $('.hamburger'),
            $nav = $('#site-nav'),
            $masthead = $('#masthead')

        $hamburger.click(function () {
            $(this).toggleClass('is-active')
            $nav.toggleClass('is-active')
            $masthead.toggleClass('is-active')
            return false
        })
    })
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#masthead').addClass('header-fixed')
            $('.breadcrumb').addClass('breadFixed')
        } else {
            $('#masthead').removeClass('header-fixed')
            $('.breadcrumb').removeClass('breadFixed')
        }
    })

    $('.single-item').slick({
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 1500,
        fade: true,
        cssEase: 'linear',
    })

    $('.content-vinos-bodega.gastronomia .item').slice(9).hide()

    let mincount = 9
    let maxcount = 18

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 50) {
            $('.content-vinos-bodega.gastronomia .item').slice(mincount, maxcount).slideDown(900)

            mincount = mincount + 6
            maxcount = maxcount + 6

        }
    })

    $('#abrir1').click(function () {
        $('#tlegales').fadeIn(300)
    })

    $('#close i').click(function () {
        $('#tlegales').fadeOut(300)
        console.log('click')
    })

    //Formulario
    $('form#registro .form-row-inner .celular').keypress(function (event) {
        // console.log(event.which);
        if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
            event.preventDefault()
        }
    })

    $('form#registro .form-row-inner .nombre').keypress(function (event) {
        const regex = new RegExp('^[a-zA-Z Ññ]+$')

        const key = String.fromCharCode(!event.charCode ? event.which : event.charCode)

        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    })

    $('form#registro .form-row-inner .apellido').keypress(function (event) {
        const regex = new RegExp('^[a-zA-Z Ññ]+$')

        const key = String.fromCharCode(!event.charCode ? event.which : event.charCode)

        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    })

    $('form#registro .form-row-inner .email').on('keypress', function () {
        let re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value)
    })

    $('.fade').slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 1500,
        fade: true,
        cssEase: 'linear',
    })

    //Slider Banners
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 8000,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        autoplay: true,
    })

    $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        autoplay: true,
        autoplaySpeed: 8000,
        dots: true,
        arrows: true,
        prevArrow: '<div class="prev"><img src="images/arrow_prev.png"></div>',
        nextArrow: '<div class="next"><img src="images/arrow_next.png"></div>',
        centerMode: true,
        focusOnSelect: true,
        infinite: true,
        centerPadding: '0px',
    })

    // Slider Access
    let alterClass = function () {
        let ww = document.body.clientWidth

        if (ww < 767) {
            $('.access').addClass('center')
        } else if (ww >= 768) {
            $('.access').removeClass('center')
        }
    }

    $(window).resize(function () {
        alterClass()
    })

    //Fire it when the page first loads:
    alterClass()

    $('.center').slick({
        dots: true,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        infinite: true,
        autoplay: true,
        centerMode: true,
        centerPadding: '35px',
        autoplaySpeed: 4000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    })
})

function mostrarError(control, mensaje) {
    $('#' + control).show()
    $('#' + control).html(mensaje)
}

$(function () {

    // $('#btnRegistro').click(function (verifL) {
    //     let nombre = $('#nombres').val()
    //     let apellido = $('#apellidos').val()
    //     let tipoDoc = $('#tipoDoc').val()
    //     let dni = $('#dni').val()
    //     let email = $('#email').val()
    //     let tyc = $('#tyc').prop('checked')
    //
    //     if (nombre.length === 0) {
    //         mostrarError('lbNombres', 'Ingresa tus nombres.')
    //         return false
    //     } else if (apellido.length === 0) {
    //         mostrarError('lbApellidos', 'Ingresa tus apellidos.')
    //         return false
    //     } else if (tipoDoc === '') {
    //         mostrarError('lbTipoDoc', 'Elige el tipo de documento.')
    //         return false
    //     } else if ((tipoDoc === 'DNI' && dni.length !== 8) || (tipoDoc !== 'DNI' && dni.length < 8)) {
    //         mostrarError('lbDNI', 'Ingresa un número de documento válido.')
    //         return false
    //     } else if (!verifEmail(email)) {
    //         mostrarError('lbEmail', 'Ingresa un email válido.')
    //         return false
    //     } else if (!tyc) {
    //         mostrarError('lbTYC', 'Acepta los términos y condiciones.')
    //         return false
    //     } else {
    //         try {
    //             $('#btnRegistro').hide()
    //             validarDNI(dni, nombre, apellido, email, tipoDoc, tyc)
    //         } catch (ex) {
    //             return false
    //         }
    //     }
    // })


    function ocultarErrores() {
        $('#lbNombres').hide()
        $('#lbApellidos').hide()
        $('#lbTipoDoc').hide()
        $('#lbDNI').hide()
        $('#lbEmail').hide()
        $('#lbTYC').hide()
    }

    function verifL(n) {
        // let permitidos = /[^áàäâªÁÀÂÄéèëêÉÈÊËíìïîÍÌÏÎóòöôÓÒÖÔ'úùüûÚÙÛÜabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ .]/
        //
        // if (permitidos.test(n.value.slice(n.value.length - 1))) {
        //     //n.value = n.value.slice(0, n.value.length - 1)
        //     n.value = ''
        //     //n.focus();
        // }
    }

    function verifTelefono(n) {

        let permitidos = /[^áàäâªÁÀÂÄéèëêÉÈÊËíìïîÍÌÏÎóòöôÓÒÖÔ'úùüûÚÙÛÜabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ .]/

        if (!permitidos.test(n.value.slice(n.value.length - 1))) {
            //n.value = n.value.slice(0, n.value.length - 1)
            n.value = ''
            //n.focus();
        }
        if (n.value.length > 9) {
            n.value = n.value.slice(0, 9)
        }
    }

    function verifDNI(n) {

        let permitidos = /[^áàäâªÁÀÂÄéèëêÉÈÊËíìïîÍÌÏÎóòöôÓÒÖÔ'úùüûÚÙÛÜabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ .]/

        if (!permitidos.test(n.value.slice(n.value.length - 1))) {
            //n.value = n.value.slice(0, n.value.length - 1)
            n.value = ''
            // n.focus();
        }
        if (n.value.length > 11) {
            n.value = n.value.slice(0, 11)
        }
    }

    function verifEmail(n) {
        return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(n)
    }

    function validarDNI(dni, nombre, apellido, email, tipoDoc) {
        let dniValid

        $.ajax({
                url: '/register',
                type: 'POST',
                data: {
                    'document_nro': dni,
                    'first_name': nombre,
                    'last_name': apellido,
                    'document_type': tipoDoc,
                    'email': email,
                },
            })
            .done(function (respuesta) {
                //console.log(respuesta);
                let resultado = JSON.parse(respuesta)

                if (resultado.status === 1) {
                    dniValid = true

                    //validarEmail(nombre, email, celular);
                    window.open('gracias.html', '_self')
                } else {
                    dniValid = false

                    mostrarError('lbDNI', resultado.mensaje)

                    $('a[href$=\'next\']').show()

                    return false
                }
            })
            .fail(function () {
                //console.log("error");
                dniValid = false

                $('a[href$=\'next\']').show()

                return false
            })
    }

})
