
 jQuery(document).ready(($) => {
        $('.quantity-control').on('click', '.plus', function(e) {
            let $input = $(this).prev('quantity-input');
            let val = parseInt($input.val());
            $input.val( val+1 ).change();
            if (val === 10) {
                $input.val( 10 ).change();
            }
        });

        $('.quantity-control').on('click', '.minus',
            function(e) {
            let $input = $(this).next('input.qty');
            var val = parseInt($input.val());
            if (val > 1) {
                $input.val( val-1 ).change();
            }
        });
 });
