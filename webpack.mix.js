const mix = require('laravel-mix')

let productionSourceMaps = false

// noinspection JSUnresolvedFunction
mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps(productionSourceMaps, 'source-map')
    .version()
    .options({
        processCssUrls: false,
    })

